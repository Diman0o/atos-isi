<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Главная страница</title>
        <link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Все задачи </h4>
                    </div>
                </div>
				
				<div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Все задачи</h3>
                            </div>
							<div class="panel-body">
								<a href="index.php?act=task_all&sub=active" class="btn btn-default waves-effect m-b-5">Активные</a>
								<a href="index.php?act=task_all&sub=rate" class="btn btn-default waves-effect m-b-5">На оценке</a>
								<a href="index.php?act=task_all&sub=arhiv" class="btn btn-default waves-effect m-b-5">Выполненные</a>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
												<thead>
													<tr>
														<th>Название</th>
														<th>Время</th>
														<th>Статус</th>
														<th>Приоритет</th>
														<th>Действие</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$qwery = mysql_query("SELECT * FROM `$db_name`.`task_list` ORDER BY id DESC") or die(mysql_error());
														$row = mysql_num_rows($qwery);
														if($row > 0){
															$mass = mysql_fetch_array($qwery);
															do{
																$x1 = $mass['datestart'];
																$x2 = $mass['dateend'];
																$x3 = date("j.n.Y");
																
																printf ('
																	<tr>
																		<td><a href="index.php?act=task_look&id=%s">%s</a></td>
																		<td><span class="badge">+'.rand(1,9).'</span></td>
																		<td><span class="badge"> Обычный</span></td>
																		<td><span class="label label-success">'.$x3.'</span></td>
																		<td class="text-center">
																			<a href="index.php?act=task_look&id=%s" class="btn btn-sm btn-info">Просмотр</a>
																			<a href="index.php?act=task_edit&id=%s" class="btn btn-sm btn-warning">Изменить</a>
																			<button type="button" class="btn btn-sm btn-danger">Удалить</button>
																		</td>
																	</tr>
																',$mass['id'],$mass['name'],$mass['id'],$mass['id']);
															}while ($mass = mysql_fetch_array($qwery));
														}else{
															echo '
																<tr>
																	<td colspan="6">Записей нет</td>	
																</td>
															';
														}
													?>										
												</tbody>
											</table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="tmp/js/jquery.min.js"></script>
        <script src="tmp/js/bootstrap.min.js"></script>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>