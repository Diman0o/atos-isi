<?php
	$id = $_SESSION['auth'];
	$sql_up = mysql_query("UPDATE `$db_name`.`task_user` SET `look` = '1' WHERE `user_id` = '$id';");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="shortcut icon" href="tmp/images/favicon_1.ico">
		<title>Профиль задачи</title>
		<link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/components.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<?php include"menu.php"?>
		<div class="wrapper">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h4 class="page-title">Страница задачи</h4>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Описание задачи</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
												<?php
													$t_id = $_GET['id'];
													$query_t = mysql_query("SELECT * FROM `$db_name`.`task_list` WHERE id = '$t_id'");
													$tmass = mysql_fetch_array($query_t);
												?>
										<?php
										$avtor_id = $tmass['avtor'];
										if($avtor_id==$_SESSION['auth'])
											{printf('<div class="col-md-3"><a href="index.php?act=task_edit&id=%s" class="btn btn-warning">Изменить задачу</a></div></br></br>',$t_id);
										};
										?>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead>
													<tr>
														<th colspan="2">Описание зачачи</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Название</td>
														<td><?=$tmass['name']?></td>
													</tr>
													<tr>
														<td>Описание</td>
														<td><?=$tmass['info']?></td>
													</tr>
													<tr>
														<td>Приоритет</td>
														<td>
														<?php
															$rol = $tmass['prior'];
															if($rol == '1'){
																echo "Низкий";
															}elseif($rol == 2){
																echo "Обычный";
															}elseif($rol == 3){
																echo "Высокий";
															}
														?>
														</td>
													</tr>
													<tr>
														<td>Статус</td>
														<td>
														<?php
															$st = $tmass['status'];
															if($st == '1'){
																echo "Активный";
															}elseif($st == 2){
																echo "На проверке";
															}elseif($st == 3){
																echo "Выполненный";
															}
														?>
														</td>
													</tr>
													<tr>
														<td>Дата начала</td>
														<td><?=$tmass['datestart']?></td>
													</tr>
													<tr>
														<td>Дата конца</td>
														<td><?=$tmass['dateend']?></td>
													</tr>
													<tr>
														<td>Автор</td>
														<?php
															$avtor_id = $tmass['avtor'];
															$sql_avtor = mysql_query("SELECT * FROM `$db_name`.`trans_users` WHERE id = '$avtor_id'");
															$av_name = mysql_fetch_array($sql_avtor);
															$id_us = $av_name['id'];
															$fname = $av_name['fname'];
															$lname = $av_name['lname'];
														?>
														<td>
															<a href="<?php   echo 'http://ponysijy.bget.ru/index.php?act=profile_user&id='.$id_us.'&sub=info'?>"><?php echo "$fname $lname";?></a>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead>
													<tr>
														<th colspan="2">Прикреплённые файлы</th>
													</tr>
												</thead>
												<tbody>
													
													<?php
														//$id_ispolnit = $isp_mass['user_id'];
														$file_sql  = mysql_query("SELECT * FROM `$db_name`.`files` WHERE type = '1' AND mod_id = '$t_id'") or die(mysql_error());
														$file_rows = mysql_num_rows($file_sql);
														$file_mass = mysql_fetch_array($file_sql);
														if($file_rows > 0){
															do{
																printf('
																	<tr>
																		<td><a href="../files/%s" target="_blank">%s</a></td>
																		<td class="text-center">
																			<a href="../files/%s" class="btn btn-fill btn-info btn-xs" target="_blank">Скачать</a>
																		</td>
																	</tr>
																',$file_mass['name'],$file_mass['name'],$file_mass['name']);
															}while($file_mass = mysql_fetch_array($file_sql));
														}else{
															echo '
																<tr>
																	<td colspan="2">Файлы отсутствуют</td>
																</tr>
															';
														}
													?>
														<tr>
															<form action="scr/file_look_add.php" method="POST" role="form" enctype="multipart/form-data">
																<td><input type="file" name="logo"></td>
																<input type="hidden" name="filetype" value="1">
																<input type="hidden" name="task_id" value="<?=$_GET['id']?>">
																<td><button class="btn btn-success btn-xs">Добавить файл</button></td>
															</form>
														</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead>
													<tr>
														<th colspan="2">Исполнители задачи</th>
													</tr>
												</thead>
												<tbody>
													
													<?php
														$query_ips = mysql_query("SELECT * FROM `$db_name`.`task_user` WHERE task_id = '$t_id'");
														$isp_mass = mysql_fetch_array($query_ips);
														
														do{
															$id_ispolnit = $isp_mass['user_id'];
															$qwery_ipd = mysql_query("SELECT * FROM `$db_name`.`trans_users` WHERE id = '$id_ispolnit'") or die(mysql_error());
															$mass__ipd = mysql_fetch_array($qwery_ipd);
															$user_rows = mysql_num_rows($qwery_ipd);
															if($user_rows > 0){
																printf('
																	<tr>
																		<td>%s %s</td>
																		<td class="text-center">
																			<a href="index.php?act=profile_user&id=%s&sub=info" class="btn btn-fill btn-info btn-xs">Просмотр</a>
																			<!--<button type="button" class="btn btn-fill btn-danger btn-xs" rel="tooltip" title="Удалить">Удалить</button>-->
																		</td>
																	</tr>
																',$mass__ipd['fname'],$mass__ipd['lname'],$mass__ipd['id']);
															}else{
																echo '
																	<tr>
																		<td colspan="2">Исполнители не назначены</td>
																	</tr>
																';
															}
														}while($isp_mass = mysql_fetch_array($query_ips));
														
													?>
												</tbody>
											</table>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<?php
										
										if ($pr==1 and $st!=3 ){printf('
										<!--<div class="col-md-4"><a href="#" class="btn btn-warning">Добавить исполнителя</a></div>-->
										<div class="col-md-6"><a href="index.php?act=task_value&id=%s" class="btn btn-warning">Оценить выполнение задачи</a></div>
										',$t_id);}
										elseif($pr!=1 and $st==1){printf('
										<div class="col-md-3"><a href="scr/task_completion.php?id=%s" class="btn btn-warning">Завершить</a></div>',$t_id);}
										else{echo '<div class="col-md-3"></div>';};
										?>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-mg-8 col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Комментарии</h3>
							</div>
							<div class="panel-body" id="scroll_down" style="height:515px; overflow: auto;">
								<div class="chat-conversation">
									<ul class="conversation-list nicescroll">
									<?php
									$id = $_GET['id'];
									$qwery_comments = mysql_query("SELECT * FROM `$db_name`.`task_comments` WHERE id_recipient = '$id' ORDER BY id") or die(mysql_error());//$id это номер отображённой задачи
									$mass_comments = mysql_fetch_array($qwery_comments);
									$mass_comments_row = mysql_num_rows($qwery_comments);
									
									if($mass_comments_row > '0'){
										do{
											$qwery = mysql_query("SELECT * FROM `$db_name`.`trans_users` WHERE id = ".$mass_comments['id_sender']." ORDER BY id") or die(mysql_error());//$id_user это session пользователя
											$mass_comments_users = mysql_fetch_array($qwery);
											if($mass_comments['id_sender'] !== $_SESSION['auth']){
												printf('
													<li class="clearfix">
														<div class="chat-avatar">
															<img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png" alt="male">
															<i>%s:%s</i>
														</div>
														<div class="conversation-text">
															<div class="ctext-wrap">
																<i>%s %s</i>
																<p>%s</p>
															</div>
														</div>
													</li>	
												',$mass_comments['h'],$mass_comments['min'],$mass_comments_users['lname'],$mass_comments_users['fname'],$mass_comments['text']);
											}else{
												printf('
													<li class="clearfix odd">
														<div class="chat-avatar">
															<img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png" alt="male">
															<i>%s:%s</i>
														</div>
														<div class="conversation-text">
															<div class="ctext-wrap">
																<i>%s %s</i>
																<p>%s</p>
															</div>
														</div>
													</li>
													',$mass_comments['h'],$mass_comments['min'],$mass_comments_users['lname'],$mass_comments_users['fname'],$mass_comments['text']);
											}
										}while($mass_comments = mysql_fetch_array($qwery_comments));
									}
									?>
								</div>
							</div>
							<div class="row">
								<form role="form" method="POST" action="../scr/task_com_add.php">
                                    <div class="col-xs-8 chat-inputbar">
										<textarea name="text" class="form-control"></textarea>
										<input name="id" type="hidden" class="form-control" value="<?=$id;?>">
									</div>
									<div class="col-xs-4 chat-send">
										<button type="submit" class="btn btn-purple waves-effect waves-light">Добавить</button>
									</div><br><br>
                                </form>
							</div>
						</div>
					</div>
					
					
				</div>
				<footer class="footer text-right">
					<div class="container">
						<div class="row">
							<div class="col-xs-6">
								2016 © Вектор
							</div>
							<div class="col-xs-6">
								<ul class="pull-right list-inline m-b-0">
									<li>
										<a href="#">Помощь</a>
									</li>
									<li>
										<a href="#">Контакты</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</footer>
			</div>
		</div>
		<script type="text/javascript">
			window.onload = function(){
				var block = document.getElementById("scroll_down");
				block.scrollTop = block.scrollHeight;
			}
		</script>
		<script src="tmp/js/jquery.min.js"></script>
		<script src="tmp/js/bootstrap.min.js"></script>
		<script src="tmp/js/jquery.app.js"></script>
	</body>
</html>