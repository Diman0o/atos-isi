<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 26.08.2016
 * Time: 23:46
 */?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
    <title>Календарь</title>

    <link href="tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/core.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/components.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/pages.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/menu.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/responsive.css" rel="stylesheet" type="text/css">


    <link href='http://fullcalendar.io/js/fullcalendar-2.8.0/fullcalendar.css' rel='stylesheet' />
    <link href='http://fullcalendar.io/js/fullcalendar-2.8.0/fullcalendar.print.css' rel='stylesheet' media='print' />
    <script src='http://fullcalendar.io/js/fullcalendar-2.8.0/lib/moment.min.js'></script>
    <script src='http://fullcalendar.io/js/fullcalendar-2.8.0/lib/jquery.min.js'></script>
    <script src='http://fullcalendar.io/js/fullcalendar-2.8.0/fullcalendar.min.js'></script>

    <script>
        $(document).ready(function() {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв.','Фев.','Март','Апр.','Май','Июнь','Июль','Авг.','Сент.','Окт.','Ноя.','Дек.'],
                dayNames: ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
                dayNamesShort: ["ВС","ПН","ВТ","СР","ЧТ","ПТ","СБ"],
                buttonText: {
                    prevYear: "&nbsp;&lt;&lt;&nbsp;",
                    nextYear: "&nbsp;&gt;&gt;&nbsp;",
                    today: "С",
                    month: "М",
                    week: "Н",
                    day: "Д"
                },contentHeight: 550,
                defaultDate: '<? echo $now_date=date('Y-m-d' )?>',
                editable: true,timeFormat: 'H(:mm)',
                eventLimit: true, // allow "more" link when too many events
                events: [<?php include('scr/calendar.php')?>]
            });
        });
    </script>
</head>
<body>
<?php include"menu.php"?>
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div id='calendar'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-md-3">
                        <div class="widget">
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <a href="#" data-toggle="modal" data-target="#add-category" class="btn btn-lg btn-primary btn-block waves-effect waves-light">
                                            <i class="fa fa-plus"></i> Create New
                                        </a>
                                        <div id="external-events" class="m-t-20">
                                            <br>
                                            <p>Drag and drop your event or click in the calendar</p>
                                            <div class="external-event bg-inverse" data-class="bg-inverse" style="position: relative">
                                                <i class="fa fa-move"></i>My Event One
                                            </div>
                                            <div class="external-event bg-danger" data-class="bg-danger" style="position: relative">
                                                <i class="fa fa-move"></i>My Event Two
                                            </div>
                                            <div class="external-event bg-primary" data-class="bg-primary" style="position: relative">
                                                <i class="fa fa-move"></i>My Event Three
                                            </div>
                                            <div class="external-event bg-purple" data-class="bg-purple" style="position: relative">
                                                <i class="fa fa-move"></i>My Event Four
                                            </div>
                                        </div>

                                        <div class="checkbox checkbox-primary m-t-40">
                                            <input id="drop-remove" type="checkbox">
                                            <label for="drop-remove">
                                                Remove after drop
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    -->
                </div>

                <div class="modal fade none-border" id="event-modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><strong>Add Event</strong></h4>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                                <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade none-border" id="add-category">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><strong>Add</strong> a category</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">Category Name</label>
                                            <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Choose Category Color</label>
                                            <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                                <option value="success">Success</option>
                                                <option value="danger">Danger</option>
                                                <option value="primary">Primary</option>
                                                <option value="warning">Warning</option>
                                                <option value="inverse">Inverse</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer text-right">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        2016 © Вектор
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- jQuery  -->
<script src="tmp/js/jquery.min.js"></script>
<script src="tmp/js/bootstrap.min.js"></script>
<script src="tmp/js/jquery.app.js"></script>
<script src="http://moltran.coderthemes.com/menu_2/assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
</body>
</html>
