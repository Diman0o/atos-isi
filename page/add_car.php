<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Добавление задачи</title>
        <!--
		<link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		-->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		
        <link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" href="../tmp/multselect/css/bootstrap-select.css">
		<script src="../tmp/js/jquery.min.js"></script>
		<script src="../tmp/js/bootstrap.min.js"></script>
		<script src="../tmp/multselect/js/bootstrap-select.js"></script>
		
		<script type="text/javascript" src="../tmp/datetime/js/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="../tmp/datetime/js/bootstrap-datetimepicker.min.js"></script>
		<link href="../tmp/datetime/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
				<div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Добавление машины</h3></div>
                            <div class="panel-body">
                                <form action="scr/add_car.php" method="POST" role="form">
                                    <div class="form-group">
									<label>Тип</label>
                                        <select  name="type" class="form-control">
                                            <option   value="1">Самосвал</option>
											<option   value="2">Трейлер</option>
											<option   value="3">Каток</option>
											<option   value="4">Бульдозер</option>
											<option   value="5">Автомобиль</option>
											<option   value="6">Экскаватор</option>
											<option   value="7">Погрузчик</option>
											<option   value="8">Асфальтоукладчик</option>
											<option   value="9">Гидромолот</option>
											<option   value="10">Кран</option>
                                        </select>
                                    </div>
									<div class="form-group">
                                        <label>Название / Модель</label>
                                        <input name="model" type="text" class="form-control">
                                    </div>
									<div class="form-group">
                                        <label>Государствтенный номер</label>
                                        <input name="gos" type="text" class="form-control">
                                    </div>
									
									<div class="form-group">
                                        <label>Ответственный</label>
                                       <!--тестпирую силект с поиском-->
                                        <div>

                                            <select name="bos" data-placeholder="Choose a Country..." class="form-control" style="width:350px;" tabindex="2">
                                                <option value=""></option>


                                                <?

                                                $qr_result = mysql_query("SELECT * FROM `$db_name`.`trans_users`")
                                                or die(mysql_error());
                                                while($data = mysql_fetch_array($qr_result)){

                                                    echo '<option value="'.$data['id'].'">'.$data['lname'].$data['fname'].'</option>';

                                                }

                                                ?>
                                                

                                            </select>
                                        </div>





                                        <!--тестпирую силект с поиском-->



                                    </div>
									<div class="form-group">
                                        <label>Описание </label>
                                        <textarea name="sut" class="form-control"></textarea>
                                    </div>
									
									<div class="form-group">
                                        <label>Год выпуска </label>
                                        <input name="year" type="text" class="form-control">
                                    </div>
									<div class="form-group">
                                        <label>Следующее ТО </label>
                                        <input name="sto" type="date" class="form-control">
                                    </div>
									
									<div class="form-group">
                                        <label>Страховка до </label>
                                        <input name="kasko" type="date" class="form-control">
                                    </div>
									
                                    <button type="submit" class="btn btn-purple waves-effect waves-light">Добавить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div><script src="tmp/js/jquery.app.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>

        <script src="tmp/js/prism.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript">
            var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
        </script>

    </body>
</html>