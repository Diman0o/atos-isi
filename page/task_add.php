<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Добавление задачи</title>
        <!--
		<link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		-->
		
		
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		
        <link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" href="../tmp/multselect/css/bootstrap-select.css">
		<script src="../tmp/js/jquery.min.js"></script>
		<script src="../tmp/js/bootstrap.min.js"></script>
		<script src="../tmp/multselect/js/bootstrap-select.js"></script>
		
		<script type="text/javascript" src="../tmp/datetime/js/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="../tmp/datetime/js/bootstrap-datetimepicker.min.js"></script>
		<link href="../tmp/datetime/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    </head>
    <body>
        
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
				<div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Изменение задачи</h3></div>
                            <div class="panel-body">
                                <form action="scr/task_add.php" method="POST" role="form" enctype="multipart/form-data">
									<div class="form-group">
										<div class="col-lg-6">
											<label>Дата начала</label>
											<div class="form-group">
												<div class="input-group date" id="datetimepicker1">
													<input name="datestart"  type="text" class="form-control" />
													<span class="input-group-addon">
															  <span class="glyphicon glyphicon-calendar"></span>
															</span>
												</div>
											</div>
											<script type="text/javascript">
												$(function () {
													//Установим для виджета русскую локаль с помощью параметра language и значения ru
													$('#datetimepicker1').datetimepicker(
														{language: 'ru'}
													);
												});
											</script>
										</div>
										<div class="col-lg-6">
											<label>Дата окончания</label>
											<div class="form-group">
												<div class="input-group date" id="datetimepicker2">
													<input  name="dateend" type="text" class="form-control" />
													<span class="input-group-addon">
															  <span class="glyphicon glyphicon-calendar"></span>
															</span>
												</div>
											</div>

											<script type="text/javascript">
												$(function () {
													//Установим для виджета русскую локаль с помощью параметра language и значения ru
													$('#datetimepicker2').datetimepicker(
														{language: 'ru'}
													);
												});
											</script>

										</div>
									</div>
                                    <div class="form-group">
                                        <label>Название задачи</label>
                                        <textarea id="name" name="name" type="text" class="form-control"  rows="1"required></textarea>
                                    </div>
									<div class="form-group">
                                        <label>Описание</label>
                                        <textarea name="info" id="about" class="form-control" rows="3" required></textarea>
                                    </div>
									<div class="form-group">
                                        <label>Прикрепить файл</label>
                                        <input type="file" name="logo" class="form-control">
                                        <input type="hidden" name="filetype" value="1">
                                    </div>
									<div class="form-group">
                                        <label>Исполнитель</label><br>
										<div class="btn-group" data-toggle="buttons">
											
											<?php
												if($pr == 1 or $pr == 2 or $pr == 3 ){
													echo '
														<label class="btn btn-default active">
															<input type="radio" name="options" value="l" onchange="check()" CHECKED> Группа
														</label>
														<label class="btn btn-default">
															<input type="radio" name="options" value="2" onchange="check()"> Специалист
														</label>
														<label class="btn btn-default">
															<input type="radio" name="options" value="3" onchange="check()"> Моя задача
														</label>	
													';
												}else{
													echo '		<label class="btn btn-default">
															<input type="radio" name="options" value="2" onchange="check()"> Рабочий
														</label>
														<label class="btn btn-default">
															<input type="radio" name="options" value="3" onchange="check()"> Моя задача
														</label>													

															
															
													';
												} //<style>#but1{display: none;}</style>
											?>

										</div>
										<br>
										<?php
										if($pr == 1 or $pr == 2 or $pr == 3 ){
										?>
										<div id="but1" class="">
											<br>
									<!-- ФОРМА ГРУППЫ -->
											<select  name="group"  class="selectpicker" data-live-search="true" data-size="5">
												<?php
													$qwer_select = mysql_query("SELECT * FROM `$db_name`.`user_group` ORDER BY id DESC") or die(mysql_error());
													$mass = mysql_fetch_array($qwer_select);
													do{
														printf ('
															<option value="%s">%s</option>
														',$mass['id'],$mass['name']);
													}while ($mass = mysql_fetch_array($qwer_select));
												?>
											</select>
										</div>
										<div id="but2" class="hidden">
											<br>
											<div class="bs-docs-example">
									<!-- ФОРМА ПОЛЬЗОВАТЕЛИ -->
											  <select name="users[]"  class="selectpicker" multiple name="selc2" data-live-search="true" data-size="5" data-width="50%">
												<?php
													$qwery_user = mysql_query("SELECT * FROM `".$db_name."`.`trans_users`  WHERE status = '0' ORDER BY id DESC") or die(mysql_error());
													$colvo  = mysql_num_rows($qwery_user);
													
													if($colvo > 0){
														$mass_user = mysql_fetch_array($qwery_user);
														//$y = 0;
														do{
															printf ('
															<option value="%s">%s %s</option>
															',$mass_user['id'],$mass_user['fname'],$mass_user['lname']);
														}while ($mass_user = mysql_fetch_array($qwery_user));
													}
												?>
											  </select>
											</div>
										</div>
									<!-- ФОРМА МОЯ ЗАДАЧА -->
										<div id="but3" class="hidden">
											<br>
											<input name="selc3" type="radio" value="<?=$_SESSION['auth'];?>" CHECKED> Моя задача
										</div>
											<?
										}else{
										?><div id="but2" class="hidden">
											<br>
											<div class="bs-docs-example">
												<!-- ФОРМА ПОЛЬЗОВАТЕЛИ -->
												<select name="users[]"  class="selectpicker" multiple name="selc2" data-live-search="true" data-size="5" data-width="50%">
													<?php
													$qwery_user = mysql_query("SELECT * FROM `".$db_name."`.`trans_users` ORDER BY id DESC") or die(mysql_error());
													$colvo  = mysql_num_rows($qwery_user);

													if($colvo > 0){
														$mass_user = mysql_fetch_array($qwery_user);
														//$y = 0;
														do{
															printf ('
															<option value="%s">%s %s</option>
															',$mass_user['id'],$mass_user['fname'],$mass_user['lname']);
														}while ($mass_user = mysql_fetch_array($qwery_user));
													}
													?>
												</select>
											</div>
										</div>
										<!-- ФОРМА МОЯ ЗАДАЧА -->
										<div id="but3" class="hidden">
											<br>
											<input name="selc3" type="radio" value="<?=$_SESSION['auth'];?>" CHECKED> Моя задача
										</div><?
										}
										?>
										<script>
											<?php
												if($pr == 1 or $pr == 2 or $pr == 3 ){
													?>
														//для групп
														function check(){
														 var rarr=document.getElementsByName("options");
														 if(rarr[0].checked){

														 document.getElementById('but1').className = '';
														 document.getElementById('but2').className = 'hidden';
														 document.getElementById('but3').className = 'hidden';
														 }
														 if(rarr[1].checked){
														 document.getElementById('but1').className = 'hidden';
														 document.getElementById('but2').className = '';
														 document.getElementById('but3').className = 'hidden';
														 }
														 if(rarr[2].checked){
														 document.getElementById('but1').className = 'hidden';
														 document.getElementById('but2').className = 'hidden';
														 document.getElementById('but3').className = '';
														 }
														 }
													<?
												}else{
													?>
														//без групп
														function check(){
														  var rarr=document.getElementsByName("options");

															if(rarr[0].checked){

																document.getElementById('but2').className = '';
																document.getElementById('but3').className = 'hidden';

															}
															if(rarr[1].checked){
																document.getElementById('but2').className = 'hidden';
																document.getElementById('but3').className = '';

															}
														}
													<?
												}
											?>



										/* lдля того что  бы работали остальное
										 function check(){
										  var rarr=document.getElementsByName("options");

										  if(rarr[1].checked){
											document.getElementById('but2').className = '';
											document.getElementById('but3').className = 'hidden';
										  }
										  if(rarr[2].checked){
											document.getElementById('but2').className = 'hidden';
											document.getElementById('but3').className = '';
										  }
										 }*/
										</script>
										<br>



										<div class="form-group">
											<label>Приоритет</label><br>
											<select name="prior" class="form-control">
												<option value="2">Обычный</option>
												<option value="1">Низкий</option>
												<option value="3">Высокий</option>
											</select>
										</div>

                                    </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light">Добавить задачу</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
<script src="../tmp/js/autosize.js"></script>
		<script>
			autosize(document.querySelectorAll('#about'));
			autosize(document.querySelectorAll('#name'));

		</script>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>