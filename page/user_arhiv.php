<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Главная страница</title>
        <link href="../../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../../tmp/css/menu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right">
                            <?
                            if($pr==1){
                                echo '
								<a href="index.php?act=user_add" class="btn btn-success">Добавить пользователя</a> 
								<a href="index.php?act=user_home" class="btn btn-default">Активные</a>';
                            }
                            ?>
                        </div>
                        <h4 class="page-title">Удалённые пользователи</h4>
                    </div>
                </div>
				
				<div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">

                                <h3 class="panel-title">Список удалённых пользователей</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Фото</th>
                                                        <th>ФИО</th>
                                                        <th>Должность</th>
                                                        <th>Мне</th>
                                                        <th>Мной</th>
                                                        <th>Действие</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
													<?php
													//ПОЛЬЗОВАТЕЛИ
														//$id = $_GET['id'];
														$qwery_user = mysql_query("SELECT * FROM `".$db_name."`.`trans_users` WHERE status = '1' ORDER BY id DESC") or die(mysql_error());
														$colvo  = mysql_num_rows($qwery_user);
														
														if($colvo > 0){
															$mass_user = mysql_fetch_array($qwery_user);
															do{
																#Должность
																$d = $mass_user['dolznost'];
																if($d == ""){$d = "Не указано";}
																##
																
																#Авы
																$uz_new_id = $mass_user['id'];
																if (file_exists('files/avatar/'.$uz_new_id.'.png')){
																	$foto = '<a href="index.php?act=profile_user&id='.$uz_new_id.'&sub=info"><img src="files/avatar/'.$uz_new_id.'.png" class="img-thumbnail" alt="profile-image" height="50" width="50"></a>';
																}elseif(file_exists('files/avatar/'.$uz_new_id.'.jpg')){
																	$foto = '<a href="index.php?act=profile_user&id='.$uz_new_id.'&sub=info"><img src="files/avatar/'.$uz_new_id.'.jpg" class="img-thumbnail" alt="profile-image" height="50" width="50"></a>';
																}elseif(file_exists('files/avatar/'.$uz_new_id.'.jpeg')){
																	$foto = '<a href="index.php?act=profile_user&id='.$uz_new_id.'&sub=info"><img src="files/avatar/'.$uz_new_id.'.jpeg" class="img-thumbnail" alt="profile-image" height="50" width="50"></a>';
																}elseif(file_exists('files/avatar/'.$uz_new_id.'.gif')){
																	$foto = '<a href="index.php?act=profile_user&id='.$uz_new_id.'&sub=info"><img src="files/avatar/'.$uz_new_id.'.gif" class="img-thumbnail" alt="profile-image" height="50" width="50"></a>';
																}else{
																	$foto = '<a href="index.php?act=profile_user&id='.$uz_new_id.'&sub=info"><img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png" class="img-thumbnail" alt="profile-image" height="50" width="50"></a>';
																}
																##
																//$prv = $mass_user['id'];
																//$qwery_user = mysql_query("SELECT * FROM `".$db_name."`.`trans_users_type` ORDER BY id DESC") or die(mysql_error());
                                                                $qwery_num_fom = mysql_query("SELECT * FROM `$db_name`.`task_list`,`$db_name`.`task_user` 
                                                                WHERE task_list.id=task_user.task_id AND task_user.user_id=$uz_new_id AND task_list.avtor=$id")
                                                                or die(mysql_error());
                                                                $qwery_num_from = mysql_query("SELECT * FROM `$db_name`.`task_list`,`$db_name`.`task_user` 
                                                                WHERE task_list.id=task_user.task_id AND task_user.user_id=$id AND task_list.avtor=$uz_new_id")
                                                                or die(mysql_error());
                                                                $num_from= mysql_num_rows($qwery_num_fom);
                                                                $num_fom = mysql_num_rows($qwery_num_from);
																
																#Кнопка удаления пользователей
																if($pr == 1){
																	$a = '<button class="btn btn-fill btn-warning btn-xs" data-toggle="modal" data-target="#myModal'.$uz_new_id.'">Востановить</button>';
																}elseif($pr == 1){
																	$a = '';
																}
																
																printf ('
																<tr>
																	<td>%s</td>
																	<td>'.$foto.'</td>
																	<td><a href="index.php?act=profile_user&id=%s&sub=info">%s %s</a></td>
																	<td>'.$d.'</td>
																	<td><a href="index.php?act=task_for_me&id_user='.$uz_new_id.'">'.$num_fom.'</a></td>
																	<td><a href="index.php?act=task_from_me&id_user='.$uz_new_id.'">'.$num_from.'</a></td>
																	<td class="text-center">
																		<a href="index.php?act=profile_user&id=%s&sub=info" class="btn btn-fill btn-info btn-xs" rel="tooltip" title="Просмотр">Просмотр</a>
																		<!--<button type="button" class="btn btn-fill btn-warning btn-xs" rel="tooltip" title="Изменить">Изменить</button>-->
																		'.$a.'
																	</td>
																</tr>
																
																<div class="modal fade" id="myModal'.$uz_new_id.'" role="dialog">
																	<div class="modal-dialog modal-sm">
																	  <div class="modal-content">
																		<div class="modal-header">
																		  <button type="button" class="close" data-dismiss="modal">&times;</button>
																		  <h4 class="modal-title">Восстановление</h4>
																		</div>
																		<div class="modal-body">
																		  <p>Востановить пользователя?</p>
																		</div>
																		<div class="modal-footer">
																		  <a href="scr/user_live.php?id='.$uz_new_id.'" class="btn btn-success">Востановить</a>
																		  <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
																		</div>
																	  </div>
																	</div>
																  </div>
																</div>
																
																',$mass_user['id'],$mass_user['id'],$mass_user['fname'],$mass_user['lname'],$mass_user['id'],$mass_user['id'],$mass_user['id']);
																unset($foto);
															}while ($mass_user = mysql_fetch_array($qwery_user));
														}else{
															echo "Пользователи отсутствуют<br><br>";
														}
													?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="tmp/js/jquery.min.js"></script>
        <script src="tmp/js/bootstrap.min.js"></script>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>