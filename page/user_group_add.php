<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Добавление задачи</title>
        <!--
		<link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		-->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		
        <link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" href="../tmp/multselect/css/bootstrap-select.css">
		<script src="../tmp/js/jquery.min.js"></script>
		<script src="../tmp/js/bootstrap.min.js"></script>
		<script src="../tmp/multselect/js/bootstrap-select.js"></script>
		
		<script type="text/javascript" src="../tmp/datetime/js/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="../tmp/datetime/js/bootstrap-datetimepicker.min.js"></script>
		<link href="../tmp/datetime/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
				<div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Добавление группы</h3></div>
                            <div class="panel-body">
                                <form action="scr/group_add.php" method="POST" role="form">
                                    <div class="form-group">
                                        <label>Название группы</label>
                                        <input name="name" type="text" class="form-control">
                                    </div>
									<div class="form-group">
										<div class="bs-docs-example">
											<label>Исполнитель</label><br>
											<select name="users[]"  class="selectpicker" multiple name="selc2" data-live-search="true" data-size="5" data-width="50%">
													<?php
														$qwery_user = mysql_query("SELECT * FROM `".$db_name."`.`trans_users` ORDER BY id DESC") or die(mysql_error());
														$colvo  = mysql_num_rows($qwery_user);
														
														if($colvo > 0){
															$mass_user = mysql_fetch_array($qwery_user);
															//$y = 0;
															do{
																printf ('
																<option value="%s">%s %s</option>
																',$mass_user['id'],$mass_user['fname'],$mass_user['lname']);
															}while ($mass_user = mysql_fetch_array($qwery_user));
														}
													?>
											</select>
										</div>
                                    </div>
                                    <button type="submit" class="btn btn-purple waves-effect waves-light">Добавить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>