<?php
	session_start();
	$id = $_GET['id'];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Главная страница</title>
        <link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/components.css" rel="stylesheet" type="text/css">
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Задача </h4>
                    </div>
                </div>
				
				<div class="row">
                    <div class="col-lg-6">
                        <ul class="nav nav-tabs tabs tabs-top">
                            <li class="active tab">
                                <a href="#home-21" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                                    <span class="hidden-xs">Информация</span>
                                </a>
                            </li>
                            <li class="tab">
                                <a href="#profile-21" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                                    <span class="hidden-xs">Исполнители</span>
                                </a>
                            </li>
                            <li class="tab">
                                <a href="#messages-21" data-toggle="tab" aria-expanded="true">
                                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                    <span class="hidden-xs">Комментарии</span>
                                </a>
                            </li>
                            <li class="tab">
                                <a href="#settings-21" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-cog"></i></span>
                                    <span class="hidden-xs">Настройки</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="home-21">
								<p><b>Название:</b> Новая задача</p>
								<p><b>Описание:</b> Новая задача</p>
								<p><b>Приоритет:</b> Новая задача</p>
								<p><b>Дата начала:</b> Новая задача</p>
								<p><b>Дата окончания:</b> Новая задача</p>
                            </div>
                            <div class="tab-pane" id="profile-21">
                                <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                        <th>Age</th>
                                                        <th>City</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                        <td>20</td>
                                                        <td>Cityname</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                        <td>20</td>
                                                        <td>Cityname</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                        <td>20</td>
                                                        <td>Cityname</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Steve</td>
                                                        <td>Mac Queen</td>
                                                        <td>@steve</td>
                                                        <td>20</td>
                                                        <td>Cityname</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="messages-21">  
										
                            </div>
                            <div class="tab-pane" id="settings-21">
                                <p>4 Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                            </div>
                        </div>
                    </div>
									<!--
									<?
									//Comments 
								
									$qwery_comments = mysql_query("SELECT * FROM `$db_name`.`task_comments` WHERE id_recipient = '$id' ORDER BY id") or die(mysql_error());//$id это номер отображённой задачи
									$mass_comments = mysql_fetch_array($qwery_comments);
									$mass_comments_row = mysql_num_rows($qwery_comments);
									?>
								<div class="col-lg-4">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">Комментарии = <?=$_SESSION['auth']?> = <?=$_SESSION['prava']?></h3>
												</div>
												<div class="panel-body" id="scroll_down" style="height:515px; overflow: auto;">
													<div class="chat-conversation">
														<ul class="conversation-list nicescroll">
									<?php
									if($mass_comments_row > '0'){
										do{
											$qwery = mysql_query("SELECT * FROM `$db_name`.`trans_users` WHERE id = ".$mass_comments['id_sender']." ORDER BY id") or die(mysql_error());//$id_user это session пользователя
											$mass_comments_users = mysql_fetch_array($qwery);
											if($mass_comments['id_sender'] == $_SESSION['auth']){
												printf('
													
																<li class="clearfix">
																	<div class="chat-avatar">
																		<img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png" alt="male">
																		<i>%s:%s</i>
																	</div>
																	<div class="conversation-text">
																		<div class="ctext-wrap">
																			<i>%s %s</i>
																			<p>
																			   %s
																			</p>
																		</div>
																	</div>
																</li>
															
												',$mass_comments['h'],$mass_comments['min'],$mass_comments_users['lname'],$mass_comments_users['fname'],$mass_comments['text']);
											}else{
												printf('
													<li class="clearfix odd">
														<div class="chat-avatar">
															<img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png" alt="male">
															<i>%s:%s</i>
														</div>
														<div class="conversation-text">
															<div class="ctext-wrap">
																<i>%s %s</i>
																<p>
																   %s
																</p>
															</div>
														</div>
													</li>
													',$mass_comments['h'],$mass_comments['min'],$mass_comments_users['lname'],$mass_comments_users['fname'],$mass_comments['text']);
											}
										}while($mass_comments = mysql_fetch_array($qwery_comments));
									}?>
									
									</div></div>
									<div class="row">
										<form action="scr/task_com_add" method="POST">
											<div class="col-xs-8 chat-inputbar">
                                                <input name="text" type="text" class="form-control">
                                                <input name="id" type="hidden" class="form-control" value="<?=$id;?>">
                                            </div>
                                            <div class="col-xs-4 chat-send">
												<input type="submit" value="Добавить" class="btn btn-purple waves-effect waves-light">
                                            </div>
										</form>
									</div>
										<br>
                                    </div>
                                </div>-->
                </div>
<style type="text/css">
    	
body{margin-top:20px;}

.content-item {
    padding:30px 0;
	background-color:#FFFFFF;
}

.content-item.grey {
	background-color:#F0F0F0;
	padding:50px 0;
	height:100%;
}

.content-item h2 {
	font-weight:700;
	font-size:35px;
	line-height:45px;
	text-transform:uppercase;
	margin:20px 0;
}

.content-item h3 {
	font-weight:400;
	font-size:20px;
	color:#555555;
	margin:10px 0 15px;
	padding:0;
}

.content-headline {
	height:1px;
	text-align:center;
	margin:20px 0 70px;
}

.content-headline h2 {
	background-color:#FFFFFF;
	display:inline-block;
	margin:-20px auto 0;
	padding:0 20px;
}

.grey .content-headline h2 {
	background-color:#F0F0F0;
}

.content-headline h3 {
	font-size:14px;
	color:#AAAAAA;
	display:block;
}


#comments {
    box-shadow: 0 -1px 6px 1px rgba(0,0,0,0.1);
	background-color:#FFFFFF;
}

#comments form {
	margin-bottom:30px;
}

#comments .btn {
	margin-top:7px;
}

#comments form fieldset {
	clear:both;
}

#comments form textarea {
	height:100px;
}

#comments .media {
	border-top:1px dashed #DDDDDD;
	padding:20px 0;
	margin:0;
}

#comments .media > .pull-left {
    margin-right:20px;
}

#comments .media img {
	max-width:100px;
}

#comments .media h4 {
	margin:0 0 10px;
}

#comments .media h4 span {
	font-size:14px;
	float:right;
	color:#999999;
}

#comments .media p {
	margin-bottom:15px;
	text-align:justify;
}

#comments .media-detail {
	margin:0;
}

#comments .media-detail li {
	color:#AAAAAA;
	font-size:12px;
	padding-right: 10px;
	font-weight:600;
}

#comments .media-detail a:hover {
	text-decoration:underline;
}

#comments .media-detail li:last-child {
	padding-right:0;
}

#comments .media-detail li i {
	color:#666666;
	font-size:15px;
	margin-right:10px;
}

    </style>
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="tmp/js/jquery.min.js"></script>
        <script src="tmp/js/bootstrap.min.js"></script>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>