<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Редактирование автомобиля</title>
        <!--
		<link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		-->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		
        <link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" href="../tmp/multselect/css/bootstrap-select.css">
		<script src="../tmp/js/jquery.min.js"></script>
		<script src="../tmp/js/bootstrap.min.js"></script>
		<script src="../tmp/multselect/js/bootstrap-select.js"></script>
		
		<script type="text/javascript" src="../tmp/datetime/js/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="../tmp/datetime/js/bootstrap-datetimepicker.min.js"></script>
		<link href="../tmp/datetime/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    </head>
    <body>
		<?php 
			include"menu.php";
			$id_auto = $_GET['id_auto'];
			
			include ('"http://'.$_SERVER['HTTP_HOST'].'config.php"');

mysql_query("set names utf8");

// соединяемся с сервером базы данных
$connect_to_db = mysql_connect($db_loc, $db_user, $db_pass)
or die("Could not connect: " . mysql_error());

// подключаемся к базе данных
mysql_select_db($db_name, $connect_to_db)
or die("Could not select DB: " . mysql_error());
$qr_result4 = mysql_query("SELECT * FROM auto_list WHERE id =".$id_auto)
or die(mysql_error());
$data4 = mysql_fetch_array($qr_result4)
		?>
        <div class="wrapper">
            <div class="container">
				<div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Добавление машины</h3></div>
                            <div class="panel-body">
                                <form action="scr/update_car.php?id_auto=<?=$id_auto?>" method="POST" role="form">
                                    <div class="form-group">
									<label>Тип</label>
                                        <?php
											$type1='Самосвал';
                                            $type2='Трейлер';
                                            $type3='Каток';
                                            $type4='Бульдозер'; 
											$type5='Автомобиль';
                                            $type6='Экскаватор';
                                            $type7='Погрузчик';
                                            $type8='Асфальтоукладчик';
											$type9='Гидромолот';
                                            $type10='Кран';
											if($data4['tipe']==1){$action1='selected';}
                                            elseif($data4['tipe']==2){$action2='selected';}
                                            elseif($data4['tipe']==3){$action3='selected';}
                                            elseif($data4['tipe']==4){$action4='selected';}
											elseif($data4['tipe']==5){$action5='selected';}
                                            elseif($data4['tipe']==6){$action6='selected';}
                                            elseif($data4['tipe']==7){$action7='selected';}
											elseif($data4['tipe']==8){$action8='selected';}
                                            elseif($data4['tipe']==9){$action9='selected';}
                                            elseif($data4['tipe']==10){$action10='selected';}
                                            
										//selected
										?>
										<select  name="type" class="form-control">
													<option disabled>Выберите тип</option>
													<!--<option <? echo $action1;?>  value="1">Асфальтоукладчик</option>

													<option <? echo $action2;?> value="2">Экскаватор</option>

													<option <? echo $action3;?> value="3">Трактор</option>

													<option <? echo $action4;?> value="4">Бульдозер</option>

													<option <? echo $action5;?>  value="5">Муха</option>-->
													
													<option <? echo $action1;?>  value="1">Самосвал</option>
													<option <? echo $action2;?>  value="2">Трейлер</option>
													<option <? echo $action3;?>  value="3">Каток</option>
													<option <? echo $action4;?>  value="4">Бульдозер</option>
													<option <? echo $action5;?>  value="5">Автомобиль</option>
													<option <? echo $action6;?>  value="6">Экскаватор</option>
													<option <? echo $action7;?>  value="7">Погрузчик</option>
													<option <? echo $action8;?>  value="8">Асфальтоукладчик</option>
													<option <? echo $action9;?>  value="9">Гидромолот</option>
													<option <? echo $action10;?>  value="10">Кран</option>
													
													
										</select>
                                    </div>
									<div class="form-group">
                                        <label>Название / Модель</label>
                                        <input name="model" type="text" class="form-control" value="<? echo $data4['model'];?>">
                                    </div>
									<div class="form-group">
                                        <label>Гос номер</label>
                                        <input name="brig" type="text" class="form-control" value="<? echo $data4['gos'];?>">
                                    </div>
									<div class="form-group">
                                        <label>Ответственный</label>
                                        <input name="brig" type="text" class="form-control" value="<? echo $data4['brig'];?>">
                                    </div>
									<div class="form-group">
                                        <label>Описание </label>
                                        <textarea name="sut" class="form-control"><? echo $data4['sut'];?></textarea>
                                    </div>
									
									<div class="form-group">
                                        <label>Год выпуска </label>
                                        <input name="year" type="text" class="form-control" value="<? echo $data4['year'];?>">
                                    </div>
									<div class="form-group">
                                        <label>Следующее ТО </label>
                                        <input name="sto" type="date" class="form-control" value="<? echo $data4['servis'];?>">
                                    </div>
									
									<div class="form-group">
                                        <label>Страховка до </label>
                                        <input name="kasko" type="date" class="form-control" value="<? echo $data4['kasko'];?>">
										</div>
									
                                    <button type="submit" class="btn btn-purple waves-effect waves-light">Редактировать</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>