<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Профиль пользователя</title>
        <link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
				<?php
						$gid = $_SESSION['auth'];
						$query = mysql_query("SELECT * FROM `$db_name`.`trans_users` WHERE id = '$gid'");
						$mass = mysql_fetch_array($query);
				?>
				<div class="row">
                    <div class="col-md-4">
                        <div class="row">
							<div class="col-sm-12">
								<div class="bg-picture text-center" style="background-image:url('http://moltran.coderthemes.com/menu_2/assets/images/big/bg.jpg')">
									<div class="bg-picture-overlay"></div>
									<div class="profile-info-name">
										<?php
											$ses_id = $_SESSION['auth'];
											$sql_ava = mysql_query("SELECT * FROM `$db_name`.`files_avatar` WHERE user_id = '$ses_id'");
											$ava_col = mysql_num_rows($sql_ava);
											if($ava_col == 1){
												$mass_ava = mysql_fetch_array($sql_ava);
												$ava_name = $mass_ava['name'];
												echo '<img src="files/avatar/'.$ava_name.'" class="thumb-lg img-circle img-thumbnail" alt="profile-image" height="100" width="100">';
											}elseif(file_exists("files/avatar/$ava_name")){
												echo '<img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png" class="thumb-lg img-circle img-thumbnail" alt="profile-image" height="100" width="100">';
											}
										?>
										<h3 class="text-white"><?php echo $mass['fname']." ".$mass['lname']?></h3>
									</div>
								</div>
							</div><br><br>
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row">
											<div class="panel-body">
												<?php
													if($ava_col == 1){
												?>
													<p>Аватар: <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#smallModal">Удалить аватар</button></p>
													<div id="smallModal" class="modal fade" tabindex="-1" role="dialog">
													  <div class="modal-dialog modal-sm">
														<div class="modal-content">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
															<h4 class="modal-title">Удаление</h4>
														  </div>
														  <div class="modal-body">
															Удалить аватар?
														  </div>
														  <div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
															<a href="../scr/avatar_delete.php?type=<?=$ses_id?>" class="btn btn-primary">Удалить</a>
														  </div>
														</div>
													  </div>
													</div>

												<?php
													} else {
												?>
													<form action="scr/avatar_load.php" method="POST" role="form" enctype="multipart/form-data">
														<p>Аватар: <input type="file" name="logo" class="form-control"><button type="submit" class="btn btn-default btn-sm">Загрузить</button></p>
													</form>
												<?php
													}
												?>
												<hr>
												<p>Был активен: <?=$mass['last_auth']?></p>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-12">
												
													<a href="index.php?act=profile&id=<?=$gid?>&sub=info" class="btn btn-default waves-effect waves-light" role="button">Информация</a>
													<!--
													<a href="index.php?act=profile&id=<?=$gid?>&sub=proj" class="btn btn-default waves-effect waves-light" role="button">Оценки</a>
													<a href="index.php?act=profile&id=<?=$gid?>&sub=activ" class="btn btn-default waves-effect waves-light" role="button">Активность</a>
													<a href="index.php?act=profile&id=<?=$gid?>&sub=proj" class="btn btn-default waves-effect waves-light" role="button">Проекты</a>
													-->
													<a href="index.php?act=profile&id=<?=$gid?>s&sub=set" class="btn btn-default waves-effect waves-light" role="button">Настройки</a>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
					<div class="col-md-8">
						<?php
							if(isset($_GET['sub'])){
								$sub = $_GET['sub'];
								if($sub == "info"){
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Информация</h3>
										</div>
										<div class="panel-body">
												<div class="about-info-p">
													<strong>Фамилия</strong>
													<br>
													<p class="text-muted"><?=$mass['lname']?></p>
												</div>
												<div class="about-info-p">
													<strong>Имя</strong>
													<br>
													<p class="text-muted"><?=$mass['fname']?></p>
												</div>
												<div class="about-info-p">
													<strong>Должность</strong>
													<br>
													<p class="text-muted"><?=$mass['dolznost']?></p>
												</div>
												<div class="about-info-p">
													<strong>Телефон</strong>
													<br>
													<p class="text-muted"><?=$mass['phone']?></p>
												</div>
												<div class="about-info-p">
													<strong>Дополнительная информация</strong>
													<br>
													<p class="text-muted"><?=$mass['dopinfo']?></p>
												</div>
										</div>
									</div>
									<?php
								}elseif($sub == "activ"){
									?>
									
									<?php
								}elseif($sub == "proj"){
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Оценки</h3>
										</div>
										<div class="panel-body">
											<div class="table-responsive">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th>#</th>
																		<th>Автор</th>
																		<th>Дата</th>
																		<th>Файл</th>
																		<th>Инфо</th>
																	</tr>
																</thead>
																<tbody>
																	
													<?php
														$rate_sql = mysql_query("SELECT * FROM `$db_name`.`user_ticket` WHERE user_id = '$gid'");
														$rate_mas = mysql_fetch_array($rate_sql);
														do{
															#Выбор ФИО оценивателя
															$user_added = $rate_mas['avtor_id'];
															$added_sql = mysql_query("SELECT * FROM `$db_name`.`trans_users` WHERE id = '$user_added'");
																if(mysql_num_rows($added_sql) == 1){
																	$mas = mysql_fetch_array($added_sql);
																	$fname = $mas['fname'];
																	$lname = $mas['lname'];
																	$fio = $fname." ".$lname;
																}
															#Проверка есть ли файл
															$sql_files = mysql_query("SELECT * FROM `$db_name`.`files` WHERE type = '1' AND mod_id = '".$_GET['id']."'");
																$fi_row = mysql_num_rows($sql_files);
																/*
																if($fi_row > 0){
																	$mas_file = mysql_fetch_array($sql_files);
																	$row_files = $mas_file['name'];
																	$knopka = "<a href='../files/$mas_file['name']' target='_blank'>$mas_file['name']</a>";
																}
																*/
															#Распечатка строк таблицы
															if($rate_mas['type'] == '1'){
																printf('
																	<tr class="success">
																		<td>%s</td>
																		<td>'.$fio.'</td>
																		<td>%s</td>
																		<td><a href="#" class="btn btn-fill btn-default btn-xs" target="_blank">Скачать</a></td>
																		<td>%s</td>
																	</tr>
																',$rate_mas['id'],$rate_mas['date'],$rate_mas['text'],$rate_mas['id'],$rate_mas['id'],$rate_mas['id']);
															}elseif($rate_mas['type'] == '2'){
																printf('
																	<tr class="danger">
																		<td>%s</td>
																		<td>'.$fio.'</td>
																		<td>%s</td>
																		<td><a href="#" class="btn btn-fill btn-default btn-xs" target="_blank">Скачать</a></td>
																		<td>%s</td>
																	</tr>
																',$rate_mas['id'],$rate_mas['date'],$rate_mas['text'],$rate_mas['id'],$rate_mas['id'],$rate_mas['id']);
															}
														}while($rate_mas = mysql_fetch_array($rate_sql));
														
													?>
																</tbody>
															</table>
											</div>
										</div>
									</div>
									<?php
								}elseif($sub == "set"){
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Настройки</h3>
										</div>
										<div class="panel-body">
											<form action="scr/profile_update.php" method="POST">
												<div class="form-group">
													<label for="FullName">Фамилия</label>
													<input name="lname" type="text" value="<?=$mass['lname']?>" class="form-control">
												</div>
												<div class="form-group">
													<label for="FullName">Имя</label>
													<input name="fname" type="text" value="<?=$mass['fname']?>" id="FullName" class="form-control">
												</div>
												<div class="form-group">
													<label for="FullName">Должность</label>
													<input name="dolznost" type="text" value="<?=$mass['dolznost']?>" id="FullName" class="form-control">
												</div>
												<div class="form-group">
													<label for="Email">Телефон</label>
													<input name="phone" type="text" value="<?=$mass['phone']?>" id="Email" class="form-control">
												</div>
												<div class="form-group">
													<label for="AboutMe">Дополнительная информация</label>
													<textarea name="dopinfo" style="height: 125px" id="AboutMe" class="form-control"><?=$mass['dopinfo']?></textarea>
													<input type="hidden" value="<?=$mass['id']?>">
												</div>
												<button class="btn btn-primary waves-effect waves-light w-md" type="submit">Изменить</button>
											</form>

										</div>
									</div>
									<?php
								}
							}
						?>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="tmp/js/jquery.min.js"></script>
        <script src="tmp/js/bootstrap.min.js"></script>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>