
		
		<script type="text/javascript" src="../tmp/datetime/js/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="../tmp/datetime/js/bootstrap-datetimepicker.min.js"></script>
		<link href="../tmp/datetime/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <link href="https://jquery-ui-bootstrap.github.io/jquery-ui-bootstrap/css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="https://rawgit.com/Artemeey/5ebc39370e568c34f03dce1639cabee8/raw/47db88126d546652b2b61f287b509386a0ecb167/jquery.datepicker.extension.range.min.js"></script>

                                                    Выберите период:
                                                    <br>
                                                    <input class="form-control input-sm"   name="dates" id="date_range"  readonly>
                                                    <br>
                                                    <br> Выберите несколько дат:
                                                    <br>
                                                    <input class="form-control input-sm" id="date_range_multiple"  name="date_all" readonly>

                                                    <script>
                                                        $(function() {
                                                            $('#date_range').datepicker({
                                                                range: 'period', // режим - выбор периода
                                                                numberOfMonths: 2,
                                                                onSelect: function(dateText, inst, extensionRange) {
                                                                    // extensionRange - объект расширения
                                                                    $('#date_range').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
                                                                }
                                                            });
                                                            $('#date_range_multiple').datepicker({
                                                                range: 'multiple', // режим - выбор периода
                                                                onSelect: function(dateText, inst, extensionRange) {
                                                                    // extensionRange - объект расширения
                                                                    $('#date_range_multiple').val(extensionRange.datesText.join(', '));
                                                                }
                                                            });
                                                        });
                                                    </script>