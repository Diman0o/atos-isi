<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Главная страница</title>
        <link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Добавление пользователя </h4>
                    </div>
                </div>
				
				<div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Форма добавления</h3></div>
                            <div class="panel-body">
                                <form role="form" method="POST" action="../scr/user_add.php">
                                    <div class="form-group">
                                        <label>Имя</label>
                                        <input name="fname" type="text" class="form-control" placeholder="Иван" required>
                                    </div>
									<div class="form-group">
                                        <label>Фамилия</label>
                                        <input name="lname" type="text" class="form-control" placeholder="Петров" required>
                                    </div>
									<div class="form-group">
                                        <label>Тип пользователя</label>
										<select name="prava" class="form-control" required>
										<?php
											$qwer_select = mysql_query("SELECT * FROM `$db_name`.`trans_users_type` ORDER BY id DESC") or die(mysql_error());
											$mass = mysql_fetch_array($qwer_select);
										
											do{
												printf ('
												<option value="%s">%s</option>
												',$mass['id'],$mass['name']);
											}while ($mass = mysql_fetch_array($qwer_select));
										?>
										</select>
                                    </div>
                                    <div class="form-group">
                                        <label>Логин</label>
                                        <input type="text" name="login" value="us<?=date("mdygis");?>" class="form-control" required>
                                    </div>
									<div class="form-group">
                                        <label>Пароль</label>
										<?php function generate_password($number){$arr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','V','X','Y','Z','1','2','3','4','5','6','7','8','9','0');$pass = "";for($i = 0; $i < $number; $i++){$index = rand(0, count($arr) - 1);$pass .= $arr[$index];}return $pass;}?>
                                        <input type="text" name="pass" value="<?=generate_password(6);?>" class="form-control" required>
                                    </div>
                                    <button type="submit" class="btn btn-success">Добавить</button>
                                </form>
                            </div>
                        </div>
                    </div>
					<div class="col-md-8">
						<div class="panel panel-default">
                            <div class="panel-heading"><h3 class="panel-title">Описание пользователей</h3></div>
                            <div class="panel-body">
								<br><b>Администратор:</b><br> Полный доступ ко всем разделам и функциям. Возможность ставить баллы за задачи. И оценивать специалистов.<br>
								<br><b>Пользователи№1:</b><br> 1. Имеет доступ к задачам (Может просматривать задачи которые поручили ему. Может удалять и изменять задачи которые он поставил себе). Может создавать групповые задачи.
								2. Имеет доступ к Журналу, Списку машин, Графику (Имеет все права в АВТОПАРКЕ, может создавать, удалять, изменять.)
								3. Может создавать группы пользователей.<br>
								<br><b>Пользователи№2:</b><br> Имеет доступ к задачам (Может просматривать задачи которые поручили ему. Может удалять и изменять задачи которые он поставил себе) Может создавать групповые задачи.
								2. Имеет доступ к Журналу, Списку машин, Графику, но не может изменять, и добавлять. Может только просматривать.
								3. Может создавать группы пользователей.<br>
								<br><b>Пользователи№3:</b><br> 1.Имеет доступ к задачам (Может просматривать задачи которые поручили ему. Может удалять и изменять задачи которые он поставил себе) Может создавать групповые задачи.
								2. Нет доступа к Автопарку
								3. Может создавать группы пользователей.<br>
								<br><b>Пользователи№4:</b><br> 
								1.Имеет доступ к задачам (Может просматривать задачи которые поручили ему. Может удалять и изменять задачи которые он поставил себе) Не может создавать групповые задачи.
								2. Нет доступа к Автопарку.
								3. Не может создавать группы пользователей.<br>
							</div>
						</div>
					</div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="tmp/js/jquery.min.js"></script>
        <script src="tmp/js/bootstrap.min.js"></script>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>