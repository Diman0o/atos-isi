<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Главная страница</title>
        <link href="../../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../../tmp/css/menu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Пользователи </h4>
                    </div>
                </div>
				<div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
								<?php
									# ID Группы
									$group_id = $_GET['id'];
									$qwer_select = mysql_query("SELECT * FROM `$db_name`.`user_group` WHERE id = '$group_id'") or die(mysql_error());
									$mass = mysql_fetch_array($qwer_select);
								?>
                                <h3 class="panel-title">Группа: <?=$mass['name']?></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
											<a href="index.php?act=user_add" class="btn btn-success">Добавить участника</a><br><br>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>ФИО</th>
                                                        <th>Действие</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
													<?php
													# Выборка ID участников группы
													$qwery_user = mysql_query("SELECT * FROM `".$db_name."`.`user_group_user` WHERE	group_id = '$group_id' ORDER BY id DESC") or die(mysql_error());
													# Проверка кол-ва участников
													$colvo  = mysql_num_rows($qwery_user);
													# Проверка если в группе участники
													if($colvo > 0){
														# Создание массива ID участников
														$id_mass = mysql_fetch_array($qwery_user);											
														do{	
															$id_us = $id_mass['user_id'];
															# Выборка участника
															$qwery_list = mysql_query("SELECT * FROM `".$db_name."`.`trans_users` WHERE	id = '$id_us' ORDER BY id DESC") or die(mysql_error());
															# Создание массива
															$mass_user = mysql_fetch_array($qwery_list);
															printf ('
															<tr>
																<td><a href="index.php?act=profile&id=%s&sub=info">%s %s</a></td>
																<td class="text-center">
																	<a href="index.php?act=profile_user&id=%s&sub=info" class="btn btn-fill btn-info btn-xs">Просмотр</a>
																	<!--<button type="button" class="btn btn-fill btn-danger btn-xs" rel="tooltip" title="Удалить">Исключить</button>-->
																</td>
															</tr>
															',$mass_user['id'],$mass_user['fname'],$mass_user['lname'],$mass_user['id']);
														}while ($id_mass = mysql_fetch_array($qwery_user));
													}else{
														echo 	"
																<tr>
																	<td>Пользователи отсутствуют</td>
																	<td></td>
																<tr>
																";
													}
												?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="tmp/js/jquery.min.js"></script>
        <script src="tmp/js/bootstrap.min.js"></script>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>