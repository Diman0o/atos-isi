<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Группы</title>
        <link href="../../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../../tmp/css/menu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Пользователи </h4>
                    </div>
                </div>
				<div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Группы пользователей</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
											<?php
												#Кнопка добавления группы
												if($pr !== 4){
													echo '<a href="index.php?act=user_group_add" class="btn btn-success">Добавить группу</a><br><br>';
												}
											?>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>№</th>
                                                        <th>Название</th>
                                                        <th>Кол-во чел.</th>
                                                        <th>Действие</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
													<?php
														$qwer_select = mysql_query("SELECT * FROM `$db_name`.`user_group` ORDER BY id DESC") or die(mysql_error());
														$mass = mysql_fetch_array($qwer_select);
														do{
															$prava_id = $mass['id'];
															$prava_all = mysql_query("SELECT * FROM `$db_name`.`user_group_user` WHERE group_id = '$prava_id'") or die(mysql_error());
															$prava_row = mysql_num_rows($prava_all);
															printf ('
																<tr>
																	<td>%s</td>
																	<td>%s</td>
																	<td>'.$prava_row.'</td>
																	<td>
																		<a href="index.php?act=user_group_list&id=%s" class="btn btn-xs btn-default">Просмотр</a>
																		<!--<a href="#" class="btn btn-xs btn-default">Изменить</a>-->
																	</td>
																</tr>
															',$mass['id'],$mass['name'],$mass['id']);
														}while ($mass = mysql_fetch_array($qwer_select));
													?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="tmp/js/jquery.min.js"></script>
        <script src="tmp/js/bootstrap.min.js"></script>
		<script src="tmp/js/jquery.app.js"></script>
    </body>
</html>