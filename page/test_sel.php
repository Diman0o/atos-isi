<!DOCTYPE html>
<html>
<head>
  <title>Bootstrap-select test page</title>

  <meta charset="utf-8">

  <link rel="stylesheet" href="../tmp/css/bootstrap.min.css">
  <link rel="stylesheet" href="../tmp/multselect/css/bootstrap-select.css">

  <style>
    body {
      padding-top: 70px;
    }
  </style>

  <script src="../tmp/js/jquery.min.js"></script>
  <script src="../tmp/js/bootstrap.min.js"></script>
  <script src="../tmp/multselect/js/bootstrap-select.js"></script>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Bootstrap-select usability tests</a>
    </div>
  </div>
</nav>
<div class="container">
  <hr>
	<h2 id="live-search_1">Выбор группы</h2>
	<div class="bs-docs-example no-code">
		<form action="" method="GET">
		  <select class="selectpicker" data-live-search="true" name="worker" data-size="5">
			<option>Выбрать</option>
			<option value="1">Сварщики</option>
			<option value="2">Слесари</option>
			<option value="3">Кухарки</option>
			<option value="4">Музыканты</option>
			<option value="5">Инженеры</option>
			<option value="6">Столяры</option>
			<option value="7">Плотники</option>
			<option value="8">Пилоты</option>
			<option value="9">Реперы</option>
			<option value="10">Маляры</option>
		  </select>
		  
		  <hr><hr>
		  
		  <h2 id="multiple-select-boxes">Выбор пользователей</h2>
			<div class="bs-docs-example">
			  <select class="selectpicker" multiple name="mult" data-live-search="true" data-size="5">
				<option value="11">Тамара Николаевна</option>
				<option value="22">Людмила михайловна</option>
				<option value="33">Софья Викторовна</option>
				<option value="33">Светлана Борисовна</option>
				<option value="33">Капс Хуяпс</option>
				<option value="33">Дирек Хачёвский</option>
			  </select>
			</div>
		  <br><br><input type="submit">
		</form>
	</div>
</div>
<script src="tmp/js/jquery.app.js"></script>
</body>
</html>
