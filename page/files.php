<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
    <title>Файлы</title>

    <link href="tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/core.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/components.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/pages.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/menu.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/responsive.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php include"menu.php"?>
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Мои файлы</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
										<a href="#AddModal" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="modal">Добавить файл</a>
										<form action="scr/file_add.php" method="POST" enctype="multipart/form-data">
											<div id="AddModal" class="modal fade">
												<div class="modal-dialog modal-sm">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
															<h4 class="modal-title">Добавление файла</h4>
														</div>
														<div class="modal-body">
															<label>Прикрепить файл</label>
															<input type="file" name="logo" class="form-control">
															<input type="hidden" name="user_id" value="<?=$id?>">
														</div>
													
														<div class="modal-footer">
															<button type="submit" class="btn btn-primary">Добавить</button>
															<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
														</div>
													</div>
												</div>
											</div>
										</form>
                                        <div class="table-responsive">
                                            <?php
												$sql = mysql_query("SELECT * FROM `$db_name`.`files_user` WHERE id_user = '".$id."'  ORDER BY id DESC");
												$row = mysql_num_rows($sql);
											?>
											<table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Название <?=$row?></th>
                                                        <th>Дата загрузки</th>
                                                        <th>Управление</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
													<?php
													if($row > 0){
														$mass = mysql_fetch_array($sql);
														do{
															printf('
																	<tr>
																		<td>%s</td>
																		<td>%s</td>
																		<td>%s</td>
																		<td>
																			<a href="/files/%s" target="_blank" class="btn btn-primary btn-xs">Скачать</a>
																			<button class="btn btn-fill btn-danger btn-xs" data-toggle="modal" data-target="#DelModal%s">Удалить</button>
																		</td>
																	</tr>
																	
																	<div class="modal fade" id="DelModal%s" role="dialog">
																		<div class="modal-dialog modal-sm">
																		  <div class="modal-content">
																			<div class="modal-header">
																			  <button type="button" class="close" data-dismiss="modal">&times;</button>
																			  <h4 class="modal-title">Удаление</h4>
																			</div>
																			<div class="modal-body">
																			  <p>Удалить файл?</p>
																			</div>
																			<div class="modal-footer">
																			  <a href="scr/file_del.php?id=%s" class="btn btn-success">Удалить</a>
																			  <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
																			</div>
																		  </div>
																		</div>
																	  </div>
																	</div>
																	',$mass['id'],$mass['name'],$mass['date'],$mass['name'],$mass['id'],$mass['id'],$mass['id']);
															}while($mass = mysql_fetch_array($sql));
													}else{
														echo "
															<tr>
																<td>Файлов нет</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
														";
													}
													?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade none-border" id="event-modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><strong>Add Event</strong></h4>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                                <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade none-border" id="add-category">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><strong>Add</strong> a category</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">Category Name</label>
                                            <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Choose Category Color</label>
                                            <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                                <option value="success">Success</option>
                                                <option value="danger">Danger</option>
                                                <option value="primary">Primary</option>
                                                <option value="warning">Warning</option>
                                                <option value="inverse">Inverse</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer text-right">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        2016 © Вектор
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- jQuery  -->
<script src="tmp/js/jquery.min.js"></script>
<script src="tmp/js/bootstrap.min.js"></script>
<script src="tmp/js/jquery.app.js"></script>
<script src="http://moltran.coderthemes.com/menu_2/assets/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
</body>
</html>
