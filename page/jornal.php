<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
    <title>Журнал</title>
    <link href="tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/core.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/components.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/pages.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/menu.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/responsive.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php include"menu.php"?>
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right">
                    <!--<button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Настройки <span class="m-l-5"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>-->
                </div>

                <h4 class="page-title">Механик </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Журнал в Автопарке</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12"><div class="table-responsive">
        <table class="table table-bordered table-hover">
<?php
// определяем начальные данные




// соединяемся с сервером базы данных


// подключаемся к базе данных


// выбираем все значения из таблицы "student"


$naw_data=date('Y-m-d');
$end_time = strtotime("+30 day");

$end_data = date("Y-m-d", $end_time);

$from = new DateTime($naw_data);
$to   = new DateTime($end_data);

$period = new DatePeriod($from, new DateInterval('P1D'), $to);

$arrayOfDates = array_map(
    function($item){return $item->format('Y.m.d');},
    iterator_to_array($period)
);$ssa='<br>';
$arrayOfDates1 = array_map(
    function($item){return $item->format('d/m');},
    iterator_to_array($period)
);
function flat_array($array) {
    foreach ($array as $value) {
        if(is_array($value)) {
            $result=array_merge($result ,flat_array($value));
        }
        else
        {
            $result[]=$value;
        }
    }
    return $result;
}

$data_mass = implode("\n",$arrayOfDates);
$data_mass1 = implode("\n",$arrayOfDates1);
$data_spis=explode("\n",$data_mass1);
$now_dotan=explode("\n",$data_mass);

$os1 = $now_dotan[0] ;
// выводим на страницу сайта заголовки HTML-таблицы
echo '<thead>';
echo '<tr>';

echo '
            <th>    
            <select id="digits">
                <option value="">---</option>
                <!--<option value="Асфальтоукладчик">Асфальтоукладчик</option>
                <option value="Экскаватор">Экскаватор</option>
                <option value="Трактор">Трактор</option>
                <option value="Бульдозер">Бульдозер</option>
                <option value="Виброкаток">Виброкаток</option>
                <option value="Автомобиль">Автомобиль</option>
                <option value="Фура">Фура</option>
                <option value="Камаз">Камаз</option>-->
				
				<option value="Самосвал">Самосвал</option>
				<option value="Трейлер">Трейлер</option>
				<option value="Каток">Каток</option>
				<option value="Бульдозер">Бульдозер</option>
				<option value="Автомобиль">Автомобиль</option>
				<option value="Экскаватор">Экскаватор</option>
				<option value="Погрузчик">Погрузчик</option>
				<option value="Асфальтоукладчик">Асфальтоукладчик</option>
				<option value="Асфальтоукладчик">Гидромолот</option>
				<option value="Кран">Кран</option>
				
				
				
            </select>
                                                                
            </th>
            <th >Номер</th>
			<th>Модель машины</th>';

foreach ($data_spis as $value) {
    echo '<th>'.$value.'</th>';
}
echo '</tr>';
echo '</thead>';
echo '<tbody id="target" >';
// выводим в HTML-таблицу все данные клиентов из таблицы MySQL
$qr_result1 = mysql_query("SELECT * FROM  `$db_name`.`auto_list`")
or die(mysql_error());
while($data1 = mysql_fetch_array($qr_result1)){
    echo '<tr >';
    $id=$data1['id'];
    $type1='Самосвал';
    $type2='Трейлер';
    $type3='Каток';
    $type4='Бульдозер';
    $type5='Автомобиль';
    $type6='Экскаватор';
    $type7='Погрузчик';
    $type8='Асфальтоукладчик';
    $type9='Гидромолот';
    $type10='Кран';
    if($data1['tipe']==1){$type=$type1;}
    elseif($data1['tipe']==2){$type=$type2;}
    elseif($data1['tipe']==3){$type=$type3;}
    elseif($data1['tipe']==3){$type=$type4;}
    elseif($data1['tipe']==5){$type=$type5;}
    elseif($data1['tipe']==6){$type=$type6;}
    elseif($data1['tipe']==7){$type=$type7;}
    elseif($data1['tipe']==8){$type=$type8;}
    elseif($data1['tipe']==9){$type=$type9;}
    elseif($data1['tipe']==10){$type=$type10;}
    echo '<td>' . $type . '</td>';
    echo '<td >'.$data1['gos'].'</td><td><a href="index.php?act=prof_car&id='.$data1['id'].'"> '.$data1['model'].'</a></td>';
    foreach ($now_dotan as $value) {
        $test_nun = mysql_query ("SELECT * FROM   `$db_name`.`auto_data` WHERE data_sob='".$value."' AND resurses LIKE '%".$id."%' ORDER BY id DESC  " );
        
            do{
				$conflict_id =$conflict['jornal'];
                $id_repit[]=$conflict_id;
            }while($conflict = mysql_fetch_array($test_nun));

                $id_repit = array_unique($id_repit);
                $id_repit = array_diff($id_repit, array(''));
                $comma_separated = implode(",", $id_repit);
$num_rows = mysql_num_rows( $test_nun );
            if($num_rows>1){
                $status='background:#dff908"';
                $url_dd="'index.php?act=jornal_date_one&id_jornal=".$comma_separated."'";
                echo '<td style="'.$status.'" onClick="location.href ='.$url_dd.'">';

                echo '</td>';
            }
            else{
                $ch = mysql_query ("SELECT * FROM   `$db_name`.`auto_data` WHERE data_sob='".$value."' AND resurses LIKE '%".$id."%' ORDER BY id DESC LIMIT 0,1  " );
                $arr = mysql_fetch_array ($ch);
                do
                {   $url_dd="'index.php?act=prof_sob&id=".$arr['jornal']."'";
                    if ($arr['type_sob']==1 ) {
                        $status='background:#4caf50"';
                        echo '<td style="'.$status.'onClick="location.href ='.$url_dd.'"">';
                        echo '</td>';}
                    elseif($arr['type_sob']==2){
                        $status='background:#f44336"';
                        echo '<td style="'.$status.'onClick="location.href ='.$url_dd.'"">';
                        echo '</td>';
                    }
                    else{
                        $status='background:#9E9E9E"';
                        echo '<td >';
                        echo '</td>';
                    };
                } while ($arr = mysql_fetch_array($ch));
                //)AND (in_array(2,$lola_test))
        }
        /*

        */
    }
        echo '</tr>';
}





// закрываем соединение с сервером  базы данных

?></tbody></table></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer text-right">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    2016 © Вектор
                </div>
                <div class="col-xs-6">
                    <ul class="pull-right list-inline m-b-0">
                        <li>
                            <a href="#">Помощь</a>
                        </li>
                        <li>
                            <a href="#">Контакты</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>
<!-- jQuery  --><script src="../tmp/js/filterTable.v1.0.min.js"></script>
<script>
    filterTable( document.getElementById("target"), {
            /* Фильтр для первого столбца чекбоксы: */

            0: document.getElementById("digits"),



        }
    );
</script>
<script src="tmp/js/jquery.min.js"></script>
<script src="tmp/js/bootstrap.min.js"></script>
<script src="tmp/js/jquery.app.js"></script>
</body>
</html>