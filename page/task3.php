<!DOCTYPE html>
<?
$radio = $_GET['radio'];
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
    <title>Список задач</title>
    <link href="tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/core.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/components.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/pages.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/menu.css" rel="stylesheet" type="text/css">
    <link href="tmp/css/responsive.css" rel="stylesheet" type="text/css">


</head>
<body>
<?php include"menu.php" ;#$start = microtime(true);?>
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right">
                    <button onclick="location.href = '../index.php?act=task_add'" type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Добавить задачу <span class="m-l-5"></span></button>

                </div>
                <h4 class="page-title">Мои задачи </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Мною порученные задачи </h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-xs-6 col-sm-2">

                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio"  id="inlineRadio1" value="option1" name="radioInline" checked="checked">
                                                    <label for="inlineRadio1"> Исполнитель </label>
                                                </div><br>
                                                <div class="radio radio-inline">
                                                    <input type="radio" id="inlineRadio2" value="option2" name="radioInline">
                                                    <label for="inlineRadio2"> Автор </label>
                                                </div>');


                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" id="inlineRadio1" value="option1" name="radioInline" >
                                                    <label for="inlineRadio1"> Исполнитель </label>
                                                </div><br>
                                                <div class="radio radio-inline">
                                                    <input type="radio" id="inlineRadio2" value="option2" checked="checked" name="radioInline">
                                                    <label for="inlineRadio2"> Автор </label>
                                                </div>
                            </div>

                            <div class="col-sm-2">
                                <input name="start" type="date" class="form-control" placeholder="">

                                <input name="end" type="date" class="form-control" placeholder="">
                            </div>
                            <div class="col-sm-2">
                                <select  name="na" class="form-control">
                                    <option  value="10">10</option>
                                    <option value="25">25</option>
                                    <option  value="100">100</option>

                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select name="stat" class="form-control">
                                    <option  value="0">Все</option>
                                    <option  value="1">Выполняется</option>
                                    <option  value="2">На проверку</option>
                                    <option  value="3">Выполнена</option>
                                    <option  value="4">Просрочена</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select name="sort" class="form-control">
                                    <option  value="dsc">Убыванию даты </option>
                                    <option  value="asc">Возрастанию</option>
                                </select>
                            </div>

                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-default waves-effect m-b-5">Фильтр</button><br>
                            </div>

                        </div>
                        <div style="height: 20px">
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Тип события</th>
                                    <th>Название</th>
                                    <th>Автор</th>
                                    <th>Исполнитель(и)</th>
                                    <th>Срок</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody class="list-of-posts">



                                    </td>
																	<td class="text-center">
																		<a href="index.php?act=task_look&id=' . $mass['id'] . '" class="btn btn-info btn-xs btn">Просмотр</a>
																		<a href="index.php?act=task_edit&id=' . $mass['id'] . '" class="btn btn-warning btn-xs">Изменить</a>
																		<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#smallModal'.$mass['id'].'">Удалить</button>
																	</td>
																</tr>

                                   
                                </tbody>

                            </table>
                            <? #echo "Время выполнения скрипта: ".(microtime(true) - $start)."<br>";
                            //КНОПКА НАЗАД (МОЖНО ИЗМЕНИТЬ HTML)
                            if($page > 1 AND $page !== 1){
                                $back = $page - 1;
                                echo "<a href='index.php?act=task_my&page=$back' class='btn btn-default'><</a>";
                            }
                            //КНОПКА ТЕКУЩЕЙ СТРАНИЦЫ (МОЖНО ИЗМЕНИТЬ HTML)
                            echo " <a href='#' class='btn btn-default active'>$page</a> ";
                            //КНОПКА ВПЕРЁД (МОЖНО ИЗМЕНИТЬ HTML)
                            if(($page * $nastr) < $num AND $page !== $num){
                                $next = $page + 1;
                                echo "<a href='index.php?act=task_my&page=$next' class='btn btn-default'>></a>";
                            }

                            //ВСЁ КОНЕЦ НИЖЕ НИЧЕГО НЕ ТРОГАЙ ЭТО ДРУГОЕ

                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer text-right">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    2016 © Вектор
                </div>
                <div class="col-xs-6">
                    <ul class="pull-right list-inline m-b-0">
                        <li>
                            <a href="#">Помощь</a>
                        </li>
                        <li>
                            <a href="#">Контакты</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="tmp/js/jquery.min.js"></script>
<script src="tmp/js/bootstrap.min.js"></script>
<script src="tmp/js/jquery.app.js"></script>
</body>
</html>