<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Страница</title>
        <link href="tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="tmp/css/core.css" rel="stylesheet" type="text/css">
        <link href="tmp/css/components.css" rel="stylesheet" type="text/css">
        <link href="tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="tmp/css/menu.css" rel="stylesheet" type="text/css">
        <link href="tmp/css/responsive.css" rel="stylesheet" type="text/css">
        <script src="tmp/js/modernizr.min.js"></script>
    </head>
    <body>
		<?php include"menu.php"?>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Настройки <span class="m-l-5"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <h4 class="page-title">Страница тест</h4>
                    </div>
                </div>
				
				<div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Responsive Table</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                        <th>Age</th>
                                                        <th>City</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                        <td>20</td>
                                                        <td>Cityname</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                        <td>20</td>
                                                        <td>Cityname</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                        <td>20</td>
                                                        <td>Cityname</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Steve</td>
                                                        <td>Mac Queen</td>
                                                        <td>@steve</td>
                                                        <td>20</td>
                                                        <td>Cityname</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">Помощь</a>
                                    </li>
                                    <li>
                                        <a href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!-- jQuery  -->
        <script src="tmp/js/jquery.min.js"></script>
        <script src="tmp/js/bootstrap.min.js"></script>
        <script src="tmp/js/detect.js"></script>
        <script src="tmp/js/fastclick.js"></script>
        <script src="tmp/js/jquery.blockUI.js"></script>
        <script src="tmp/js/waves.js"></script>
        <script src="tmp/js/wow.min.js"></script>
        <script src="tmp/js/jquery.nicescroll.js"></script>
        <script src="tmp/js/jquery.scrollTo.min.js"></script>
        <!-- App js -->
        <script src="tmp/js/jquery.app.js"></script>
    </body>
</html>