<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="shortcut icon" href="tmp/images/favicon_1.ico">
		<title>Профиль пользователя</title>
		<link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<?php include"menu.php"?>
		<div class="wrapper">
			<div class="container">
				<?php
					if(isset($_GET['id'])){
						$gid = $_GET['id'];
						$query = mysql_query("SELECT * FROM `$db_name`.`trans_users` WHERE id = '$gid'");
						$mass = mysql_fetch_array($query);
					}
				?>
				<div class="row">
					<div class="col-md-5">
						<div class="row">
							<div class="col-sm-12">
								<div class="bg-picture text-center" style="background-image:url('http://moltran.coderthemes.com/menu_2/assets/images/big/bg.jpg')">
									<div class="bg-picture-overlay"></div>
									<div class="profile-info-name">
										<?php
											$uz_new_id = $_GET['id'];
											if (file_exists('files/avatar/'.$uz_new_id.'.png')){
												echo '<img src="files/avatar/'.$uz_new_id.'.png" class="thumb-lg img-circle img-thumbnail" alt="profile-image" height="100" width="100">';
											}elseif(file_exists('files/avatar/'.$uz_new_id.'.jpg')){
												echo '<img src="files/avatar/'.$uz_new_id.'.jpg" class="thumb-lg img-circle img-thumbnail" alt="profile-image" height="100" width="100">';
											}elseif(file_exists('files/avatar/'.$uz_new_id.'.jpeg')){
												echo '<img src="files/avatar/'.$uz_new_id.'.jpeg" class="thumb-lg img-circle img-thumbnail" alt="profile-image" height="100" width="100">';
											}elseif(file_exists('files/avatar/'.$uz_new_id.'.gif')){
												echo '<img src="files/avatar/'.$uz_new_id.'.gif" class="thumb-lg img-circle img-thumbnail" alt="profile-image" height="100" width="100">';
											}else{
												echo '<img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/user-male-icon.png" class="thumb-lg img-circle img-thumbnail" alt="profile-image" height="100" width="100">';
											}
										?>
										<h3 class="text-white"><?php echo $mass['fname']." ".$mass['lname']?></h3>
									</div>
								</div>
							</div><br><br>
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row">
											<div class="panel-body">
												<p>Был активен: <?=$mass['last_auth']?></p>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="btn-group btn-group-justified m-b-10">
															<?php

																if($pr != 1 and $_GET['id']==$_SESSION['auth']){
																printf('
																		<a href="index.php?act=profile_user&id='.$gid.'&sub=info" class="btn btn-default waves-effect waves-light" role="button">Информация</a>
																		<a href="index.php?act=profile_user&id='.$gid.'&sub=stat" class="btn btn-default waves-effect waves-light" role="button">Статистика</a>
																	');
																	}
																	elseif($pr != 1)
																	{
																	printf('
																		<a href="index.php?act=profile_user&id='.$gid.'&sub=info" class="btn btn-default waves-effect waves-light" role="button">Информация</a>
																	');
																	}
																	else{
																	printf('
																		<div class="col-md-6">
																		<a href="index.php?act=profile_user&id='.$gid.'&sub=info" class="btn btn-default waves-effect waves-light" role="button">Информация</a>
																		<a href="index.php?act=profile_user&id='.$gid.'&sub=rate" class="btn btn-default waves-effect waves-light" role="button">Оценки</a>
																		</div>
																		<div class="col-md-6">
																		<a href="index.php?act=profile_user&id='.$gid.'&sub=stat" class="btn btn-default waves-effect waves-light" role="button">Статистика</a>
																		<a href="index.php?act=profile_user&id='.$gid.'s&sub=set" class="btn btn-default waves-effect waves-light" role="button">Настройки</a>
																		</div>
																	');
																	}
															?>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="btn-group btn-group-justified m-b-10">

													<?if($pr==1 and  $_GET['id']==$_SESSION['auth']){
														echo"";
													}elseif($pr == 1){
														printf('
														<a href="#Good"  data-toggle="modal" class="btn btn-success waves-effect waves-light" role="button">Положительно</a>
														<a href="#Bad"  data-toggle="modal" class="btn btn-danger waves-effect waves-light" role="button">Отрицательно</a>');}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<?php	
							if(isset($_GET['sub'])){
								$sub = $_GET['sub'];
								if($sub == "info"){
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Информация</h3>
										</div>
										<div class="panel-body">
                                            <div class="about-info-p">
                                                <strong>Фамилия</strong>
                                                <br>
                                                <p class="text-muted"><?=$mass['lname']?></p>
                                            </div>
                                            <div class="about-info-p">
                                                <strong>Имя</strong>
                                                <br>
                                                <p class="text-muted"><?=$mass['fname']?></p>
                                            </div>
                                            <div class="about-info-p">
                                                <strong>Должность</strong>
                                                <br>
                                                <p class="text-muted"><?=$mass['dolznost']?></p>
                                            </div>
                                            <div class="about-info-p">
                                                <strong>Телефон</strong>
                                                <br>
                                                <p class="text-muted"><?=$mass['phone']?></p>
                                            </div>
                                            <div class="about-info-p">
                                                <strong>Дополнительная информация</strong>
                                                <br>
                                                <p class="text-muted"><?=$mass['dopinfo']?></p>
                                            </div>
										</div>
									</div>
									<?php
								}elseif($sub == "stat"){
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Статистика</h3>
										</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="panel-body">
														<div class="panel-heading">
															<h3 class="panel-title">Статистика задач</h3>
														</div>
														<!--начало строительства диаграммы-->
														<?
														$roster_sql1 = mysql_query("SELECT * FROM `$db_name`.`task_user` WHERE user_id=$gid and status=1")
														or die(mysql_error());
														$roster_sql2 = mysql_query("SELECT * FROM `$db_name`.`task_user` WHERE user_id=$gid and status=2")
														or die(mysql_error());
														$roster_sql3 = mysql_query("SELECT * FROM `$db_name`.`task_user` WHERE user_id=$gid and status=3")
														or die(mysql_error());
														$all_a['1'] = mysql_num_rows( $roster_sql1 );
														$all_a['2'] = mysql_num_rows( $roster_sql2 );
														$all_a['3'] = mysql_num_rows( $roster_sql3 );
														foreach ($all_a as $key => $value){
															if($all_a[$key]==0){
																$all_a[$key]=0;
															}
														};



														?>
														<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
														<script type="text/javascript">
															google.charts.load("current", {packages:["corechart"]});
															google.charts.setOnLoadCallback(drawChart);
															function drawChart() {
																var data = google.visualization.arrayToDataTable([
																	['Task', 'Hours per Day'],
																	['Выполняет',    <?=$all_a[1]?>],
																	['Выполнил',      <?=$all_a[2]?>],
																	['Оценены',  <?=$all_a[3]?>]

																]);

																var options = {

																	pieHole: 0.0,
																};

																var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
																chart.draw(data, options);
															}
														</script>
														<div id="donutchart" style="height: 300px;"></div>
														<div class="about-info-p">
															<a href="index.php?act=task_user_stat&id_user=<?=$gid?>">Подробнее...</a>
														</div>

													<!--конец строительства диаграммы-->

													<!--<form name="form1" action="/=<?echo'index.php?act=profile_user&id'.$gid.'&sub=stat&';?>" method="GET" class='form-horizontal' role='form'>
													
													</div>
													немного говнокода
													-->
														<div class="panel-heading">
															<h3 class="panel-title">Статистика оцененных задач</h3>
														</div><?
													if(empty($_GET['start']) and empty($_GET['end']) ){
														$start='2014-07-31';$end='2018-07-31';
													}else{$start=$_GET['start'];
													$end=$_GET['end'];}
													$ds = substr($start, 8);
													$ms = substr($start, 5,2);
													$ys = substr($start, 0,4);


													$de = substr($end, 8);
													$me = substr($end, 5,2);
													$ye = substr($end, 0,4);
													$start1=$ds.'.'.$ms.'.'.$ys;
													$end1=$de.'.'.$me.'.'.$ye;
													
													?>
													<form name="form1" action="scr/statistica.php" method="GET" class='form-horizontal' role='form'>
														<div class="col-md-4">
															<input style="display: none;" type='text' class='form-control input-sm' value='<? echo $gid?>' name='id'>
															<input style="display: none;" type='text' class='form-control input-sm' value='<? echo $sub?>' name='sub'>
															<input type='date' class='form-control input-sm' placeholder='' name='start'>
															
														</div>
																		
														<div class="col-md-4">
															<input type='date' class='form-control input-sm' placeholder='<? echo $end1;?>' name='end'>
														</div>
														<div class="col-md-4">
															<button type='submit' class='btn btn-purple waves-effect waves-light'>Посчитать</button>
														</div><p>C - <? echo $start1;?>  по - <? echo $end1;?></p>
													</form>
													
													<?
													#include('scr/statistica.php');
													?>
													<div class='row'><div class='col-md 12'>
											<style>
											#chartdiv1 {
												width	: 100%;
												height	: 250px;
											}
											</style>
											<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
											<script src="https://www.amcharts.com/lib/3/serial.js"></script>
											<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>


											<div id="chartdiv1"></div>
											<script>
											var chart = AmCharts.makeChart("chartdiv1", {
												"type": "serial",
												"theme": "light",
												"marginRight": 40,
												"marginLeft": 40,
												"autoMarginOffset": 20,
												"mouseWheelZoomEnabled":false,
												"dataDateFormat": "DD.MM.YYYY",

												"balloon": {
													"borderThickness": 1,
													"shadowAlpha": 0
												},
												"graphs": [{
													"id": "g1",
													"balloon":{
														"drop":true,
														"adjustBorderColor":false,
														"color":"#ffffff"
													},
													"bullet": "round",
													"bulletBorderAlpha": 1,
													"bulletColor": "#FFFFFF",
													"bulletSize": 5,
													"hideBulletsCount": 50,
													"lineThickness": 2,
													"title": "red line",
													"useLineColorForBulletBorder": true,
													"valueField": "value",
													"balloonText": "<span style='font-size:18px;'>[[value]]</span>"
												}],

												"chartCursor": {
													"pan": true,
													"valueLineEnabled": true,
													"valueLineBalloonEnabled": true,
													"cursorAlpha":1,
													"cursorColor":"#258cbb",
													"limitToGraph":"g1",
													"valueLineAlpha":0.2,
													"valueZoomable":true
												},

												"categoryField": "date",
												"categoryAxis": {
													"parseDates": true,
													"dashLength": 1,
													"minorGridEnabled": true
												},
												"export": {
													"enabled": true
												},
												"dataProvider": [
												<?		$key=0;
														$points=0;/*
											$qwery1 = mysql_query("SELECT task_list.id, task_list.name, task_list.dateend,task_user.rate_v ,task_user.task_id FROM `$db_name`.`task_user`, `$db_name`.`task_list` WHERE task_user.user_id='".$gid."' and task_list.status='3' and task_list.dateend>'$start' and task_list.dateend<'$end' and task_list.id=task_user.task_id ORDER BY task_list.dateend ASC") or die(mysql_error());
											SELECT task_list.id, task_list.name, task_list.dateend,task_user.rate_v ,task_user.task_id FROM `task_user`, `task_list` WHERE task_user.user_id='1'and task_list.datestart>'10.10.2015'<'10.08.2016' and task_list.id=task_user.task_id ORDER BY task_list.dateend ASC
											*/
											$qwery1 = mysql_query("SELECT task_list.id, task_list.name, task_list.dateend,task_user.rate_v ,task_user.task_id FROM `$db_name`.`task_user`, `$db_name`.`task_list` WHERE task_user.user_id='$gid' and task_list.status='3' and task_list.dateend>'$start1'<'$end1' and task_list.id=task_user.task_id ORDER BY task_list.dateend DESC ") or die(mysql_error());
											$prefic = mysql_num_rows( $qwery1 );
																									while($mass1 = mysql_fetch_array($qwery1))
																									{
											$data_shot=	substr($mass1['dateend'], 0, 10); $points += $mass1['rate_v'];
											if($key==$prefic-1){$zap=' ';}else{$zap=',' ;}
											echo'{
												   "date": "'.$data_shot.'",
													"value": '.$mass1['rate_v'].'
													}'.$zap;
													$key++;
																										}
												;?>
												]
												});
											</script>
											</div></div>
			<div class='row' style="margin-top: 73px;"><div class='col-md 12'>
													<label class="col-sm-4 control-label" for="example-input-small"><? echo'Всего: '.$prefic.' А Маx возможно: '.$prefic*200;?></label>
													<div class="col-sm-8">
														<div class="progress">
			  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"><?
				echo ' '.$points;
				?>
				<span class="sr-only">
				</span>
			  </div> </div><p>
			  
			 <p>
			</div> </div>
			</div>

														<!-- Сюда пакет Яровой -->
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
								}elseif($sub == "rate"){
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Оценки</h3>
										</div>
										<div class="panel-body">
											<div class="table-responsive">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th>#</th>
																		<th>Автор</th>
																		<th>Дата</th>
																		<th>Файл</th>
																		<th>Инфо</th>
																		<th>Действие</th>
																	</tr>
																</thead>
																<tbody>
																	
													<?php
														$rate_sql = mysql_query("SELECT * FROM `$db_name`.`user_ticket` WHERE user_id = '$gid'");
														$rate_mas = mysql_fetch_array($rate_sql);
														do{
															#Выбор ФИО оценивателя
															$user_added = $rate_mas['avtor_id'];
															$added_sql = mysql_query("SELECT * FROM `$db_name`.`trans_users` WHERE id = '$user_added'");
																if(mysql_num_rows($added_sql) == 1){
																	$mas = mysql_fetch_array($added_sql);
																	$fname = $mas['fname'];
																	$lname = $mas['lname'];
																	$fio = $fname." ".$lname;
																}
															#Проверка есть ли файл
															$file_empty = $rate_mas['file_name'];
															if($file_empty == ""){
																$knopka = "---";
															}else{
																$knopka = '<a href="files/rate/'.$file_empty.'" class="btn btn-fill btn-default btn-xs" target="_blank">Скачать</a>';
															}
															/*
															$sql_files = mysql_query("SELECT * FROM `$db_name`.`files` WHERE type = '2' AND mod_id = '".$_GET['id']."'");
																$fi_row = mysql_num_rows($sql_files);
																
																if($fi_row > 0){
																	$mas_file = mysql_fetch_array($sql_files);
																	$row_files = $mas_file['name'];
																	//$knopka = "<a href='../files/$mas_file['name']' target='_blank'>$mas_file['name']</a>";
																	$knopka = '<a href="files/'.$row_files.'" class="btn btn-fill btn-default btn-xs" target="_blank">Скачать</a>';
																}else{
																	$knopka = 'Файла нет';
																}
															*/
															#Распечатка строк таблицы
															if($rate_mas['type'] == '1'){
																printf('
																	<tr class="success">
																		<td>%s</td>
																		<td>'.$fio.'</td>
																		<td>%s</td>
																		<td>'.$knopka.'</td>
																		<td>%s</td>
																		<td><a href="#RateModal%s" class="btn btn-primary btn-xs" data-toggle="modal">Удалить</a></td>
																	</tr>
																	
																	<div id="RateModal%s" class="modal fade">
																	  <div class="modal-dialog modal-sm">
																		<div class="modal-content">
																		  <div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																			<h4 class="modal-title">Удаление</h4>
																		  </div>
																		  <div class="modal-body">
																			Удалить оценку?
																		  </div>
																		  <div class="modal-footer">
																			<a href="scr/rate_delete.php?user='.$_GET["id"].'&id=%s" class="btn btn-primary">Удалить</a>
																			<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
																		  </div>
																		</div>
																	  </div>
																	</div>
																	
																',$rate_mas['id'],$rate_mas['date'],$rate_mas['text'],$rate_mas['id'],$rate_mas['id'],$rate_mas['id']);
															}elseif($rate_mas['type'] == '2'){
																printf('
																	<tr class="danger">
																		<td>%s</td>
																		<td>'.$fio.'</td>
																		<td>%s</td>
																		<td>'.$knopka.'</td>
																		<td>%s</td>
																		<td><a href="#myModal%s" class="btn btn-primary btn-xs" data-toggle="modal">Удалить</a></td>
																	</tr>
																	
																	<div id="myModal%s" class="modal fade">
																	  <div class="modal-dialog modal-sm">
																		<div class="modal-content">
																		  <div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																			<h4 class="modal-title">Удаление</h4>
																		  </div>
																		  <div class="modal-body">
																			Удалить оценку?
																		  </div>
																		  <div class="modal-footer">
																			<a href="scr/rate_delete.php?user='.$_GET["id"].'&id=%s" class="btn btn-primary">Удалить</a>
																			<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
																		  </div>
																		</div>
																	  </div>
																	</div>
																	
																',$rate_mas['id'],$rate_mas['date'],$rate_mas['text'],$rate_mas['id'],$rate_mas['id'],$rate_mas['id']);
															}
														}while($rate_mas = mysql_fetch_array($rate_sql));
														
													?>
																</tbody>
															</table>
											</div>
										</div>
									</div>
									<?php
										}elseif($sub == "set"){
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Настройки</h3>
										</div>
										<div class="panel-body">
											<form role="form" action="scr/user_update.php" method="GET">
												<div class="form-group">
													<label for="FullName">Фамилия</label>
													<input type="text" value="<?=$mass['lname']?>" name="lname" class="form-control">
												</div>
												<div class="form-group">
													<label for="FullName">Имя</label>
													<input type="text" value="<?=$mass['fname']?>" name="fname" class="form-control">
												</div>
												<div class="form-group">
													<label for="Username">Логин</label>
													<input type="text" value="<?=$mass['login']?>" name="login" class="form-control">
												</div>
												<div class="form-group">
													<label for="Password">Пароль</label>
													<input type="text" value="<?=$mass['password']?>" name="pass" class="form-control">
												</div>
												<div class="form-group">
													<label for="Username">Должность</label>
													<input type="text" value="<?=$mass['dolznost']?>" name="doljn" class="form-control">
												</div>
												<div class="form-group">
													<label for="Username">Телефон</label>
													<input type="text" value="<?=$mass['phone']?>" name="phone" class="form-control">
												</div>
												<div class="form-group">
													<label for="AboutMe">Информация</label>
													<textarea style="height: 125px" name="info" class="form-control"><?=$mass['dopinfo']?></textarea>
												</div>
												<input type="hidden" value="<?=$mass['id']?>" name="id" class="form-control">
												<button class="btn btn-primary waves-effect waves-light w-md" type="submit">Изменить</button>
											</form>

										</div>
									</div>
									<?php
								}
							}
						?>
					</div>
				</div>
				 
				<div id="Good" class="modal fade">
					<form action="scr/user_rate.php" method="POST" enctype="multipart/form-data">
						<div class="modal-dialog">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title">Похвалить пользователя</h4>
							  </div>
							  <div class="modal-body">
								<div class="form-group">
									<label>Описание похвалы</label>
									<textarea name="text" class="form-control"></textarea>
									<input name="type" type="hidden" value="1">
									<input name="id" type="hidden" value="<?=$mass['id']?>">
								</div>
								<div class="form-group">
									<label>Прикрепить файл</label>
									<input type="file" name="logo" class="form-control">
									<input type="hidden" name="filetype" value="2">
								</div>
							  </div>
							  <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
								<button type="submit" class="btn btn-primary">Добавить</button>
							  </div>
							</div>
						</div>
					</form>
				</div>
				<div id="Bad" class="modal fade">
					<form action="scr/user_rate.php" method="POST" enctype="multipart/form-data">
						<div class="modal-dialog">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title">Жалоба на пользователя</h4>
							  </div>
							  <div class="modal-body">
								<div class="form-group">
									<label>Описание жалобы</label>
									<textarea name="text" class="form-control"></textarea>
									<input name="type" type="hidden" value="2">
									<input name="id" type="hidden" value="<?=$mass['id']?>">
								</div>
								<div class="form-group">
										<label>Прикрепить файл</label>
										<input type="file" name="logo" class="form-control">
										<input type="hidden" name="filetype" value="2">
								</div>
							  </div>
							  <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
								<button type="submit" class="btn btn-primary">Добавить</button>
							  </div>
							</div>
						</div>
					</form>
				</div>
				
				<footer class="footer text-right">
					<div class="container">
						<div class="row">
							<div class="col-xs-6">
								2016 © Вектор
							</div>
						</div>
					</div>
				</footer>
			</div>
		</div>
		<script src="tmp/js/jquery.min.js"></script>
		<script src="tmp/js/bootstrap.min.js"></script>
		<script src="tmp/js/jquery.app.js"></script>
	</body>
</html>