<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="tmp/images/favicon_1.ico">
        <title>Страница конференций</title>
        <link href="../tmp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../tmp/css/core.css" rel="stylesheet" type="text/css">
		<link href="../tmp/css/menu.css" rel="stylesheet" type="text/css">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
		<?php include"menu.php";?>
        <div class="wrapper">
            <div class="container">
				
				<div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="ms-menu" style="height:695px; overflow: auto;">
								<div class="p-15">
									<div class="dropdown">
										<a class="btn btn-primary btn-block" href="" data-toggle="dropdown">Messages <i class="caret m-l-5"></i></a>
						
										<ul class="dropdown-menu dm-icon w-100">
											<li><a href=""><i class="fa fa-envelope"></i> Messages</a></li>
											<li><a href=""><i class="fa fa-users"></i> Contacts</a></li>
											<li><a href=""><i class="fa fa-format-list-bulleted"> </i>Todo Lists</a></li>
										</ul>
									</div>
								</div>
								
								<div class="list-group lg-alt">
									<?	
										$id_aut = $_SESSION['auth'];
										$qwery_msg = mysql_query("SELECT * FROM `$db_name`.`conf_user` WHERE user_id = '$id_aut' ORDER BY id") or die(mysql_error());//$id это номер отображённой задачи
										$mass_msg = mysql_fetch_array($qwery_msg);
										
										if(isset($_GET['id_sob'])){
											$id_sob = $_GET['id_sob'];
										}elseif(!isset($_GET['add_sob'])){
											$id_sob = $mass_msg['id'];
										}else{
											$add_sob = $_GET['add_sob'];
										}

										do{
											$qwery = mysql_query("SELECT * FROM `$db_name`.`conf` WHERE id = ".$mass_msg['conf_id']." ORDER BY id") or die(mysql_error());//$id_user это session пользователя
											$mass_msg_users = mysql_fetch_array($qwery);
											printf('
												<a class="list-group-item media" href="index.php?act=conf&id_sob='.$mass_msg['conf_id'].'">
													<div class="pull-left">
														<img src="http://bootdey.com/img/Content/avatar/avatar2.png" alt="" class="img-avatar">
													</div>
													<div class="media-body">
														<small class="list-group-item-heading">%s</small>
													</div>
												</a>
											',$mass_msg_users['name']);
										}while($mass_msg = mysql_fetch_array($qwery_msg));
									?>
									
								</div>
						
								
							</div>
                        </div>
                    </div>
					<div class="col-md-9">
                        <div class="panel panel-default">

                            <div class="ms-body" style="height:600px; overflow: auto;">
								<?
									$qwery_msg_l = mysql_query("SELECT * FROM `$db_name`.`conf_msg` WHERE conf_id = '$id_sob' ORDER BY id") or die(mysql_error());//$id_sob это номер отображённой задачи
									
									$mass_row = mysql_num_rows($qwery_msg_l);

									if($mass_row>0){
										$mass_msg_l = mysql_fetch_array($qwery_msg_l);
										
										do{
											$qwery_name = mysql_query("SELECT * FROM `$db_name`.`trans_users` WHERE id = ".$mass_msg_l['id_user']." ORDER BY id") or die(mysql_error());//$id_user это session пользователя
										$mass_name = mysql_fetch_array($qwery_name);
											if($mass_msg_l['id_user']==$id_aut){
												
												printf('<div class="message-feed right">
														<div class="pull-right">
															<img src="http://bootdey.com/img/Content/avatar/avatar2.png" alt="" class="img-avatar">
														</div>
														<div class="media-body">
															<div class="mf-content">
																%s
															</div>
															<small class="mf-date"><i class="fa fa-clock-o"></i> %s/%s/%s в %s:%s</small>
														</div>
													</div>
													
												',$mass_msg_l['text'],$mass_msg_l['d'],$mass_msg_l['m'],$mass_msg_l['y'],$mass_msg_l['h'],$mass_msg_l['min']);
											}else{
												printf('
													<div class="message-feed media">
														<div class="pull-left">
															<img src="http://bootdey.com/img/Content/avatar/avatar1.png" alt="" class="img-avatar">
														</div>
														<div class="media-body">
															<div class="mf-content">
																<b>%s %s:</b><br>
																%s
															</div>
															<small class="mf-date"><i class="fa fa-clock-o"></i> %s/%s/%s в %s:%s</small>
														</div>
													</div>
												',$mass_name['lname'],$mass_name['fname'],$mass_msg_l['text'],$mass_msg_l['d'],$mass_msg_l['m'],$mass_msg_l['y'],$mass_msg_l['h'],$mass_msg_l['min']);
											}
										}while($mass_msg_l = mysql_fetch_array($qwery_msg_l));
									}else{
										echo '<h3 style="padding: 12px;">Данная конференция не содержит сообщений</h3>';
									}
								?>
								<!--<div class="message-feed media">
									<div class="pull-left">
										<img src="http://bootdey.com/img/Content/avatar/avatar1.png" alt="" class="img-avatar">
									</div>
									<div class="media-body">
										<div class="mf-content">
											Quisque consequat arcu eget odio cursus, ut tempor arcu vestibulum. Etiam ex arcu, porta a urna non, lacinia pellentesque orci. Proin semper sagittis erat, eget condimentum sapien viverra et. Mauris volutpat magna nibh, et condimentum est rutrum a. Nunc sed turpis mi. In eu massa a sem pulvinar lobortis.
										</div>
										<small class="mf-date"><i class="fa fa-clock-o"></i> 20/02/2015 at 09:00</small>
									</div>
								</div>
								
								<div class="message-feed right">
									<div class="pull-right">
										<img src="http://bootdey.com/img/Content/avatar/avatar2.png" alt="" class="img-avatar">
									</div>
									<div class="media-body">
										<div class="mf-content">
											Mauris volutpat magna nibh, et condimentum est rutrum a. Nunc sed turpis mi. In eu massa a sem pulvinar lobortis.
										</div>
										<small class="mf-date"><i class="fa fa-clock-o"></i> 20/02/2015 at 09:30</small>
									</div>
								</div>
								
								<div class="message-feed media">
									<div class="pull-left">
										<img src="http://bootdey.com/img/Content/avatar/avatar1.png" alt="" class="img-avatar">
									</div>
									<div class="media-body">
										<div class="mf-content">
											Etiam ex arcumentum
										</div>
										<small class="mf-date"><i class="fa fa-clock-o"></i> 20/02/2015 at 09:33</small>
									</div>
								</div>
								
								<div class="message-feed right">
									<div class="pull-right">
										<img src="http://bootdey.com/img/Content/avatar/avatar2.png" alt="" class="img-avatar">
									</div>
									<div class="media-body">
										<div class="mf-content">
											Etiam nec facilisis lacus. Nulla imperdiet augue ullamcorper dui ullamcorper, eu laoreet sem consectetur. Aenean et ligula risus. Praesent sed posuere sem. Cum sociis natoque penatibus et magnis dis parturient montes,
										</div>
										<small class="mf-date"><i class="fa fa-clock-o"></i> 20/02/2015 at 10:10</small>
									</div>
								</div>
								
								<div class="message-feed media">
									<div class="pull-left">
										<img src="http://bootdey.com/img/Content/avatar/avatar1.png" alt="" class="img-avatar">
									</div>
									<div class="media-body">
										<div class="mf-content">
											Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam ac tortor ut elit sodales varius. Mauris id ipsum id mauris malesuada tincidunt. Vestibulum elit massa, pulvinar at sapien sed, luctus vestibulum eros. Etiam finibus tristique ante, vitae rhoncus sapien volutpat eget
										</div>
										<small class="mf-date"><i class="fa fa-clock-o"></i> 20/02/2015 at 10:24</small>
									</div>
								</div>
								-->
							</div>
							<div class="msb-reply">
								<form method="POST" action="scr/add_msg.php?id_sob=<?=$id_sob;?>">
									<textarea name="text" placeholder="Что у тебя на уме..."></textarea>
									<button type="submit"><i class="fa fa-paper-plane-o"></i></button>
								</form>
							</div>
                        </div>
                    </div>
                </div>
				<style type="text/css">
						

				#messages-main {
					position: relative;
					margin: 0 auto;
					box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
				}
				#messages-main:after, #messages-main:before {
					content: " ";
					display: table;
				}
				#messages-main .ms-menu {
					position: absolute;
					left: 0;
					top: 0;
					border-right: 1px solid #eee;
					padding-bottom: 50px;
					height: 100%;
					width: 240px;
					background: #fff;
				}
				@media (min-width:768px) {
					#messages-main .ms-body {
					padding-left: 240px;
				}
				}@media (max-width:767px) {
					#messages-main .ms-menu {
					height: calc(100% - 58px);
					display: none;
					z-index: 1;
					top: 58px;
				}
				#messages-main .ms-menu.toggled {
					display: block;
				}
				#messages-main .ms-body {
					overflow: hidden;
				}
				}
				#messages-main .ms-user {
					padding: 15px;
					background: #f8f8f8;
				}
				#messages-main .ms-user>div {
					overflow: hidden;
					padding: 3px 5px 0 15px;
					font-size: 11px;
				}
				#messages-main #ms-compose {
					position: fixed;
					bottom: 120px;
					z-index: 1;
					right: 30px;
					box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);
				}
				#ms-menu-trigger {
					user-select: none;
					position: absolute;
					left: 0;
					top: 0;
					width: 50px;
					height: 100%;
					padding-right: 10px;
					padding-top: 19px;
				}
				#ms-menu-trigger i {
					font-size: 21px;
				}
				#ms-menu-trigger.toggled i:before {
					content: '\f2ea'
				}
				.fc-toolbar:before, .login-content:after {
					content: ""
				}
				.message-feed {
					padding: 20px;
				}
				#footer, .fc-toolbar .ui-button, .fileinput .thumbnail, .four-zero, .four-zero footer>a, .ie-warning, .login-content, .login-navigation, .pt-inner, .pt-inner .pti-footer>a {
					text-align: center;
				}
				.message-feed.right>.pull-right {
					margin-left: 15px;
				}
				.message-feed:not(.right) .mf-content {
					background: #03a9f4;
					color: #fff;
					box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
				}
				.message-feed.right .mf-content {
					background: #eee;
				}
				.mf-content {
					padding: 12px 17px 13px;
					border-radius: 2px;
					display: inline-block;
					max-width: 80%
				}
				.mf-date {
					display: block;
					color: #B3B3B3;
					margin-top: 7px;
				}
				.mf-date>i {
					font-size: 14px;
					line-height: 100%;
					position: relative;
					top: 1px;
				}
				.msb-reply {
					box-shadow: 0 -20px 20px -5px #fff;
					position: relative;
					margin-top: 30px;
					border-top: 1px solid #eee;
					background: #f8f8f8;
				}
				.four-zero, .lc-block {
					box-shadow: 0 1px 11px rgba(0, 0, 0, .27);
				}
				.msb-reply textarea {
					width: 100%;
					font-size: 13px;
					border: 0;
					padding: 10px 15px;
					resize: none;
					height: 60px;
					background: 0 0;
				}
				.msb-reply button {
					position: absolute;
					top: 0;
					right: 0;
					border: 0;
					height: 100%;
					width: 60px;
					font-size: 25px;
					color: #2196f3;
					background: 0 0;
				}
				.msb-reply button:hover {
					background: #f2f2f2;
				}
				.img-avatar {
					height: 37px;
					border-radius: 2px;
					width: 37px;
				}
				.list-group.lg-alt .list-group-item {
					border: 0;
				}
				.p-15 {
					padding: 15px!important;
				}
				.btn:not(.btn-alt) {
					border: 0;
				}
				.action-header {
					position: relative;
					background: #f8f8f8;
					padding: 15px 13px 15px 17px;
				}
				.ah-actions {
					z-index: 3;
					float: right;
					margin-top: 7px;
					position: relative;
				}
				.actions {
					list-style: none;
					padding: 0;
					margin: 0;
				}
				.actions>li {
					display: inline-block;
				}

				.actions:not(.a-alt)>li>a>i {
					color: #939393;
				}
				.actions>li>a>i {
					font-size: 20px;
				}
				.actions>li>a {
					display: block;
					padding: 0 10px;
				}
				.ms-body{
					background:#fff;    
				}
				#ms-menu-trigger {
					user-select: none;
					position: absolute;
					left: 0;
					top: 0;
					width: 50px;
					height: 100%;
					padding-right: 10px;
					padding-top: 19px;
					cursor:pointer;
				}
				#ms-menu-trigger, .message-feed.right {
					text-align: right;
				}
				#ms-menu-trigger, .toggle-switch {
					-webkit-user-select: none;
					-moz-user-select: none;
				}
					</style>
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Вектор
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="tmp/js/jquery.min.js"></script>
        <script src="tmp/js/bootstrap.min.js"></script>
    </body>
</html>